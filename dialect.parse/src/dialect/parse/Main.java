package dialect.parse;

import java.io.IOException;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.generator.JavaIoFileSystemAccess;
import org.eclipse.xtext.generator.AbstractFileSystemAccess;
import org.eclipse.xtext.generator.GeneratorContext;
import org.eclipse.xtext.generator.IFileSystemAccess2;

import com.gitlab.johanvdberg.sofl_editor.SoflDslStandaloneSetup;
import com.google.inject.Injector;

public class Main { 

    public static void main(String[] args) throws IOException {
        Injector injector = new SoflDslStandaloneSetup().createInjectorAndDoEMFRegistration();
        ResourceSet rs = injector.getInstance(ResourceSet.class);
        Resource resource = rs.getResource(URI.createURI(args[0]), true);
        resource.load(null);
        IResourceValidator validator = injector.getInstance(IResourceValidator.class);
        IGenerator2 generator = injector.getInstance(IGenerator2.class);
        List<Issue> issues = validator.validate(resource,
                CheckMode.ALL, CancelIndicator.NullImpl);
        for (Issue issue: issues) {
            switch (issue.getSeverity()) {
                case ERROR:
                    System.out.println("ERROR: " + issue.getMessage());
                case WARNING:
                        System.out.println("WARNING: " + issue.getMessage());
            }
        }
        IFileSystemAccess2 fs = injector.getInstance(JavaIoFileSystemAccess.class);
        ((AbstractFileSystemAccess) fs).setOutputPath("./src");
        IGeneratorContext context = new GeneratorContext();
        if(issues.size() == 0) {
        	generator.doGenerate(resource, fs, context);
        }
    }

}
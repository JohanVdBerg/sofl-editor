package com.gitlab.johanvdberg.sofl_editor.smt

import java.util.List
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import org.eclipse.emf.ecore.EObject

class SmtPortInBehaviour extends SmtPortInNode {
	
	public SmtPort refinedPort
	
	new(SmtNode parent, int index, List<VariableType> varaible_list, SmtCondition condition, SmtPort refinedPort, EObject syntax_port){
		super(parent, index, varaible_list, condition, syntax_port)
		this.refinedPort = refinedPort
	}
}


package com.gitlab.johanvdberg.sofl_editor.smt

import java.util.HashMap
import java.util.List
import org.eclipse.emf.ecore.EObject
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligation
import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.semantics.Node.NodeType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import com.gitlab.johanvdberg.sofl_editor.types.ShaddowType

class SmtStructureNode extends SmtNode {
	
	protected new(NodeType nodeType){
		super(nodeType)
	}
	new(BroadcastStructure node, SmtModule parent){
		super(NodeType::BroadCast)
		this.flow_port_obligations = new HashMap<String, ProofObligation>
		this.parent = parent
		this.node_name = node.name
		if(node.in_variable !== null){
			this.add_port(#[new VariableType(node.in_variable.name)], SmtCondition::create(true), true, node)
			this.add_port(node.out_list.map[it|new VariableType(it.name)], SmtCondition::create(true), false, node)		
		}else{
			this.add_port(#[new ShaddowType(node.in_connect.name)], SmtCondition::create(true), true, node)
			this.add_port(node.out_connect.map[it|new ShaddowType(it.name)], SmtCondition::create(true), false, node)
		}
		
		//this.compute_invariant()
	}
	
	new(ConditionStructure node, SmtModule parent){
		super(NodeType::Condition)
		this.flow_port_obligations = new HashMap<String, ProofObligation>
		this.parent = parent		
		this.node_name = node.name
		this.add_port(#[new VariableType(node.in_variable.name)], SmtCondition::create(this, true, 0), true, node)
		var lst = node.out_list.map[it|new VariableType(it.varname.name)]
		this.add_port(lst, SmtCondition::create(this, false, 0), false, node)
		//this.compute_invariant()
	}
	
	
	new(MergeStructure node, SmtModule parent){
		super(NodeType::Merde)
		this.flow_port_obligations = new HashMap<String, ProofObligation>
		this.parent = parent
		this.node_name = node.name	
		this.add_port(node.invar.map[new VariableType(it.name)], SmtCondition::create(this, true, 0), true, node)
		this.add_port(#[new VariableType(node.out.name)], SmtCondition::create(this, true, 0), false, node.out)
		//this.compute_invariant()
	}
	

	new(UnMergeStructure node, SmtModule parent){
		super(NodeType::Unmerge)
		this.flow_port_obligations = new HashMap<String, ProofObligation>
		this.parent = parent	
		this.node_name = node.name	
		this.add_port(#[new VariableType(node.invar.name)], SmtCondition::create(this, true, 0), true, node.invar)
		this.add_port(node.out_list.map[new VariableType(it.name)], SmtCondition::create(this, true, 0), false, node)
		//this.compute_invariant()
	}		
	
	override computeProofObligations() {
		this.connect_internal_ports
		super.computeProofObligations
	}
	
	def connect_internal_ports(){
		for(iport : this.get_outer_destination_ports){
			for(oport : this.get_outer_source_ports){
				this.io_conditions.add( 
					IOPortRelation::create(iport.index, oport.index,				
						iport.condition, oport.condition))
			}		
		}
	}
	
	def add_port(List<VariableType> varaible_list, boolean input, EObject syntax_port){
		if(input){
			var p = new SmtPortStructureNode(this, this.in_ports.length, varaible_list, syntax_port)
			p.set_as_destination(true)
			this.in_ports.add(p)
		}else{
			var p = new SmtPortStructureNode(this, this.out_ports.length, varaible_list, syntax_port)
			p.set_as_destination(false)
			this.out_ports.add(p)
		}
	}
}
package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.soflDsl.Statecondition

class PrePostPredicates {
	public int in_pre_id
	public int out_post_id
	public SmtCondition pre
	public SmtCondition post
	
	
	new(Statecondition trans){
		this.in_pre_id = trans.in_port_id - 1
		this.out_post_id = trans.out_port_id - 1
		this.pre = new SmtCondition(trans.pre.condition)
		this.post = new SmtCondition(trans.post.condition)
	}
	
}
package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.types.TypeBase

class SmtLogicScopePair {
	public SmtLogicVariable variable
	public TypeBase type
	
	new(String variable, TypeBase type){
		this.type = type
		this.variable = new SmtLogicVariable(variable)
	}
	
	override toString(){
		'''(«variable.smt» «type»)'''
	}
}

package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.List
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents
import java.util.HashMap
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase

class ProofObligationCondition extends ProofObligation {
	new(List<SmtCondition> condition_list) {
		this.environment.addAll(condition_list)
	}

	override String smt_proof() {
		// var tmp = ConstantStore.store
		val file_data = new SmtFileContents

		for (out_index : 0 ..< this.environment.length - 1) {
			for (inner_index : out_index + 1 ..< this.environment.length) {
				val all_vars_scope = new HashMap<String, TypeBase>
				for (current : #[this.environment.get(out_index).variables, this.environment.get(inner_index).variables]) {
					for (key : current.keySet) {
						all_vars_scope.put(key, current.get(key))
					}
				}

				val vars = all_vars_scope.keySet.map[k|SmtCondition::create(k, all_vars_scope.get(k))]
				val head = vars.head
				val all_equal = SmtCondition::conjunction(vars.tail.map[SmtCondition::equal(head, it)])
				var cond = SmtCondition::conjunction(
					#[
						SmtCondition::conjunction(
							#[this.environment.get(out_index), this.environment.get(inner_index)]),
						all_equal
					]
				)
				file_data.add_push
				file_data.add_variables(cond)
				file_data.add_assert_formula(cond.get_formula.smt)
				file_data.add_print('''This formula must not be satisfiable port «out_index» and «inner_index»''')
				file_data.add_sat_check
				file_data.add_pop
			}
		}
		file_data.contents
	}
}

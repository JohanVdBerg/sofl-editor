package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents
import java.util.List

class ProofObligationInvariantImply extends ProofObligation {
	public SmtCondition P
	public SmtCondition Q
	public SmtCondition imply_condition
	public SmtLogicScope imply_scope
	public String text

	new(List<SmtCondition> p_list, List<SmtCondition> q_list, String text) {
		this.P = SmtCondition::conjunction(p_list)
		this.Q = SmtCondition::conjunction(q_list)

		this.imply_scope = new SmtLogicScope
		for (v : this.P.variables.keySet) {
			imply_scope.add(v, this.P.variables.get(v))
		}

		val exists_scope = new SmtLogicScope
		for (v : this.Q.variables.keySet) {
			if (!this.imply_scope.contain(v)) {
				exists_scope.add(v, this.Q.variables.get(v))
			}
		}

		if (this.imply_scope.varaible_count > 0) {
			this.imply_condition = SmtCondition::forall(imply_scope,
				SmtCondition.imply(this.P, SmtCondition::exists(exists_scope, this.Q)))
		} else {
			this.imply_condition = SmtCondition.imply(this.P, SmtCondition::exists(exists_scope, this.Q))
		}
		this.text = text
	}

	override smt_proof() {
		var file_data = new SmtFileContents

		val first_msg = '''«this.text»: Check that environment in environment-->condition is satisfiable'''
		val second_msg = '''«this.text»: Check that environment-->condition is satisfiable'''

		file_data.add_variables(this.imply_condition)
		file_data.add_variables(this.P)
		file_data.add_variables(this.Q)

		// prove the implication
		file_data.add_push
		file_data.add_assert_formula(this.P.get_formula.smt)
		file_data.add_print(first_msg)
		file_data.add_sat_check
		file_data.add_pop
		// prove the environment is satisfiable
		file_data.add_push
		file_data.add_assert_formula(this.imply_condition.get_formula.smt)
		file_data.add_print(second_msg)
		file_data.add_sat_check
		file_data.add_pop

		file_data.contents
	}
}

package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.ArrayList
import java.util.List
import com.gitlab.johanvdberg.sofl_editor.smt.SmtPort
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents


//start invariant - by super module
//environment by port of process begin refined - for all over these variables and invariant
// the target port and the invariant of the lower module given the condition to be valid.
class ProofObligationRefinementDown extends ProofObligation {
	public List<SmtCondition> refined_port_conditions
	public List<SmtCondition> upper_invariants
	public List<SmtCondition> lower_invariants	
	public List<SmtCondition> init_condition
	public List<SmtCondition> obligation
	public SmtCondition refine_imply
	public SmtCondition inv_imply
	public SmtCondition upper_inv 
	SmtCondition upper_and
	SmtCondition lower_and
	
	new(List<SmtCondition> upper_invariant, SmtPort refined_list, List<SmtCondition> target_port_conditions, Iterable<SmtCondition> lower_invariant){
		//super(target_port)
		this.upper_invariants = new ArrayList<SmtCondition>
		this.lower_invariants = new ArrayList<SmtCondition>
		this.init_condition = new ArrayList<SmtCondition>
		this.obligation = new ArrayList<SmtCondition>
		this.refined_port_conditions = #[refined_list.condition]
		
		this.obligation = target_port_conditions
		this.lower_invariants.addAll(lower_invariant)
		this.upper_invariants.addAll(upper_invariant)
		
		this.upper_inv = SmtCondition::conjunction(this.upper_invariants)
		var lower_inv = SmtCondition::conjunction(this.lower_invariants)
		
		this.upper_and = SmtCondition::conjunction(
			#[
				upper_inv, 
				SmtCondition::conjunction(this.refined_port_conditions)
			]
		)
		this.lower_and = SmtCondition::conjunction(
			#[
				lower_inv, 
				SmtCondition::conjunction(this.obligation)
			]
		)
		
		this.inv_imply = SmtCondition::forall(upper_inv, SmtCondition::imply(upper_inv, lower_inv))			
		this.refine_imply = SmtCondition::forall(upper_and, SmtCondition::imply(upper_and, lower_and))
		
					
	}
	
	override smt_proof(){
		var file_data = new SmtFileContents

		file_data.add_variables(upper_and)
		file_data.add_variables(inv_imply)
		file_data.add_variables(refine_imply)
		file_data.add_variables(lower_and)
		
		//prove upper state is not empty
		file_data.add_push
		
		file_data.add_assert_formula(upper_and.get_formula.smt)
		file_data.add_print("Test if the state space being refined is not empty")
		file_data.add_sat_check
		file_data.add_pop
		
		//check that the invariant area allowed
		file_data.add_push
		
		file_data.add_assert_formula(inv_imply.get_formula.smt)
		file_data.add_print("Check that the lower invariant are satisfiable given that the upper invariants are true")
		file_data.add_sat_check
		file_data.add_pop
		
		//prove the refinement implication
		file_data.add_push
		
		file_data.add_assert_formula(refine_imply.get_formula.smt)
		file_data.add_print("Check if the lower state space is contained by the upper (port being refined)")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.contents		
	}
}

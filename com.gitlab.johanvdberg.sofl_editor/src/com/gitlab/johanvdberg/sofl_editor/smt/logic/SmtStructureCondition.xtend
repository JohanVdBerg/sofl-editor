package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.TreeMap
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.smt.SmtNode

class SmtStructureCondition extends SmtCondition {
	
	SmtNode parent
	boolean input
	int port_index
	
	new(SmtNode parent, boolean input, int port_index){
		this.variables = new TreeMap<String, TypeBase>
		this.parent = parent
		this.input = input
		this.port_index = port_index
	}		
	
	override get_formula(){
		if(this.input){
			SmtCondition.conjunction(this.parent.get_outer_destination_ports().get(this.port_index).get_connected_ports().map[it.get_codition]).get_formula()
		}else{
			SmtCondition.conjunction(this.parent.get_outer_source_ports().get(this.port_index).get_connected_ports().map[it.get_codition]).get_formula()
		}
	}
	
}


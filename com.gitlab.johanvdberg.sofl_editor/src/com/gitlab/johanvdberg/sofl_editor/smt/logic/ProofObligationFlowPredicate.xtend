package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents
import java.util.ArrayList
import java.util.List

class ProofObligationFlowPredicate extends ProofObligation {
	public List<SmtCondition> environment
	public SmtCondition obligation
	//public Map<String, TreeSet<SmtPort>> pre_image
	
	new (List<SmtCondition> environment, SmtCondition obligation, int in_port_index, int out_port_index){
		this.obligation_name = '''Pre image in«in_port_index + 1»  out «out_port_index + 1»'''
		this.environment = new ArrayList<SmtCondition>(environment)
		this.obligation = obligation
		
	}
	
	new (List<SmtCondition> environment, SmtCondition obligation, int in_port_index){
		this.obligation_name = '''Pre image in«in_port_index + 1»  and all ouput ports'''
		this.environment = new ArrayList<SmtCondition>(environment)
		this.obligation = obligation
		
	}
	
	override smt_proof(){
		var file_data = new SmtFileContents
		var imply = if(this.environment.length > 0){
			var env = SmtCondition::conjunction(this.environment)
			//SmtLogicScope scope
			val exist_scope = new SmtLogicScope()		
			for(n_key: this.obligation.variables.keySet){
				if(!env.variables.keySet.contains(n_key)){
					exist_scope.add(n_key, this.obligation.variables.get(n_key))
				}
			}
					//prove the implication
			file_data.add_push
			file_data.add_variables(env)
			file_data.add_variables(this.obligation)
			file_data.add_assert_formula(env.get_formula.smt)
			file_data.add_print("Check if environment is satisfiable")
			file_data.add_sat_check
			file_data.add_pop
			val inner_imply = SmtCondition::imply(env, this.obligation)  
			val tmp = SmtCondition::forall(env, inner_imply)
			SmtCondition.not(SmtCondition::exists(tmp, tmp))
		}else{
			this.obligation
		}
		
	

		//prove the environment is satisfiable
		file_data.add_push
		file_data.add_variables(imply)
		file_data.add_assert_formula(imply.get_formula.smt)
		file_data.add_print("Check if the environment allows the port state")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.contents		
	}
}

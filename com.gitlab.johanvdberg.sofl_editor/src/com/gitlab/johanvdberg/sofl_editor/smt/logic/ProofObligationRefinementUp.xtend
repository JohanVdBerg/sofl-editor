package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.SmtPort
import java.util.ArrayList
import java.util.List
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents

class ProofObligationRefinementUp extends ProofObligation {
	public List<SmtCondition> upper_obligation
	public List<SmtCondition> upper_invariants
	public List<SmtCondition> lower_invariants	
	public SmtCondition lower_obligation
	
	new(List<SmtCondition> lower_condition, List<SmtCondition> lower_port_predicate, SmtPort refined_port, List<SmtCondition> upper_conditions) {
		this.upper_invariants = new ArrayList<SmtCondition>
		this.lower_invariants = new ArrayList<SmtCondition>
		this.upper_obligation = new ArrayList<SmtCondition>
		
		this.upper_obligation.add(refined_port.condition)
		this.lower_invariants.addAll(lower_condition)
		this.upper_invariants.addAll(upper_conditions)
		this.lower_obligation = SmtCondition::conjunction(lower_port_predicate)		
	}
	
	override smt_proof(){
		var file_data = new SmtFileContents
		
		var lower_inv = SmtCondition.conjunction(this.lower_invariants)
		var upper_inv = SmtCondition.conjunction(this.upper_invariants)
		var lower_and = SmtCondition.conjunction(#[lower_inv,this.lower_obligation])
		var upper_and = SmtCondition.conjunction(#[upper_inv, SmtCondition.conjunction(this.upper_obligation)])
		
		var inv_imply = SmtCondition.forall(lower_inv, SmtCondition.imply(lower_inv, upper_inv))		
		var refine_imply = SmtCondition.forall(lower_and, SmtCondition.imply(lower_and, upper_and))
		

		file_data.add_variables(lower_and)
		file_data.add_variables(upper_and)
		file_data.add_variables(inv_imply)
		file_data.add_variables(refine_imply)
		
		//prove upper state is not empty
		file_data.add_push		
		file_data.add_assert_formula(lower_and.get_formula.smt)
		file_data.add_print("Test if the refined state space is not empty")
		file_data.add_sat_check
		file_data.add_pop
		
		//check that the invariant area allowed
		file_data.add_push		
		file_data.add_assert_formula(inv_imply.get_formula.smt)
		file_data.add_print("Check that the upper invariant are satisfiable given that the lower invariants are true")
		file_data.add_sat_check
		file_data.add_pop
		
		//prove the refinement implication
		file_data.add_push		
		file_data.add_assert_formula(refine_imply.get_formula.smt)
		file_data.add_print("Check if the lower state space is contained by the upper (port begin refined)")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.contents		
	}
	
}

package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.SmtPortInNode
import java.util.List
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents
import java.util.ArrayList

class ProofObligationPredicateTransform extends ProofObligation {
	public List<SmtCondition> start_invariants
	public SmtCondition pre_condition
	public SmtCondition post_condition	
	public int in_port_index
	public int out_port_index
	public List<SmtCondition> end_invariants
	
	
	def static create(List<SmtCondition> start_invariants,List<SmtCondition> store_invariants, SmtPortInNode start_port, SmtPortInNode end_port,  List<SmtCondition> end_invariants){
		new ProofObligationPredicateTransform(start_invariants,store_invariants, start_port, end_port, end_invariants)						
	}
	
	protected new (List<SmtCondition> start_invariants,List<SmtCondition> store_invariants, SmtPortInNode in_port, SmtPortInNode out_port, List<SmtCondition> end_invariants){
		
		this.obligation_name = '''in«in_port.index + 1» out«out_port.index + 1»'''
		this.start_invariants = new ArrayList<SmtCondition> 
		this.start_invariants.addAll(start_invariants.map[it.to_previous_state])		
		this.start_invariants.addAll(store_invariants)		
		this.end_invariants =  new ArrayList<SmtCondition>
		this.end_invariants.addAll(end_invariants)
		//this.end_invariants.addAll(store_invariants)
		this.in_port_index = in_port.index
		this.out_port_index = out_port.index
		val trans = out_port.parentNode.io_conditions.filter[it| it.in_port == in_port.index && it.out_port == out_port.index]
		
		if(trans.length != 1){
			throw new RuntimeException('''only one pre/post condition is allowed «trans.length» number of pairs found in port «in_port.index + 1» out port «out_port.index + 1»''')
		}
		this.post_condition = trans.head.post_condition
		this.pre_condition = trans.head.pre_condition.to_previous_state
	}
	
	
	override smt_proof(){
		var file_data = new SmtFileContents
			
		var start = SmtCondition::conjunction(
			#[
				SmtCondition::conjunction(this.start_invariants), 
				this.pre_condition
			]
		)
		
		var end = SmtCondition::conjunction(
			#[
				SmtCondition::conjunction(this.end_invariants), 
				this.post_condition
			]
		)

		var imply = SmtCondition::forall(start, SmtCondition::imply(start, end))
		
		file_data.add_variables(start)
		file_data.add_variables(end)
		file_data.add_variables(imply)
		
		
		//prove the implication
		file_data.add_push		
		file_data.add_assert_formula(start.get_formula.smt)
		file_data.add_print("Check if pre condition and invariant is satisfiable")
		file_data.add_sat_check
		file_data.add_pop		
		
		file_data.add_push			
		file_data.add_assert_formula(end.get_formula.smt)
		file_data.add_print("Check if post condition and invariant is satisfiable")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.add_push
		var tmp = SmtCondition::forall(this.pre_condition, 
			SmtCondition::imply(
				this.pre_condition, 
				SmtCondition::conjunction(this.start_invariants)
			)
		)
		//tmp = SmtCondition.not(SmtCondition::exists(tmp, tmp))
		file_data.add_assert_formula(tmp.get_formula.smt)
		file_data.add_print("Check that the invariant is satisfiable given the pre condition is true")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.add_push
		tmp = SmtCondition::forall(this.post_condition, 
			SmtCondition::imply(
				this.post_condition, 
				SmtCondition::conjunction(this.end_invariants)
			)
		)
		//tmp = SmtCondition.not(SmtCondition::exists(tmp, tmp))
		file_data.add_assert_formula(tmp.get_formula.smt)
		file_data.add_print("Check that the invariant is satisfiable given the post condition is true")
		file_data.add_sat_check
		file_data.add_pop
		
		//prove the environment is satisfiable
		file_data.add_push
		tmp = imply
		//tmp = SmtCondition.not(SmtCondition::exists(tmp, tmp))
		file_data.add_assert_formula(tmp.get_formula.smt)
		file_data.add_print("Check if the P -> Q is satisfiable")
		file_data.add_sat_check
		file_data.add_pop
		
		file_data.contents		
	}
	
}
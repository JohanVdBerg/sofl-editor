package com.gitlab.johanvdberg.sofl_editor.smt.logic

class SmtLogicTerm extends Logic{
	
	new(Logic logic){
		super(Operation.GROUPING)
		this.term_list.add(logic)
	}	
}
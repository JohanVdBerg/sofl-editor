package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.List
import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFileContents

class ProofObligationSatisfiable extends ProofObligation {
	
	public List<SmtCondition> condition
	new(String name, List<SmtCondition> conditions) {
		this.obligation_name = name
		this.condition = new ArrayList<SmtCondition>
		this.condition.addAll(conditions)		
	}
	
	override smt_proof(){
		var file_data = new SmtFileContents
		
		//prove the environment is satisfiable
		for(cond: this.condition){
			file_data.add_push
			file_data.add_variables(cond)
			file_data.add_assert_formula(cond.get_formula.smt)
			file_data.add_print("Check if the environment is satisfiable")
			file_data.add_sat_check
			file_data.add_pop
		}
		file_data.contents		
	}
}

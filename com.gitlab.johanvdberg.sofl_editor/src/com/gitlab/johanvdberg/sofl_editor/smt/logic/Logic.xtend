package com.gitlab.johanvdberg.sofl_editor.smt.logic

import com.gitlab.johanvdberg.sofl_editor.smt.file.ConstantStore
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Set
import javax.sound.sampled.BooleanControl.Type
import javax.management.RuntimeErrorException

class Logic {
	
	enum Operation{
		AND,
		EXISTS,
		OR,
		IMPLY,
		VARIABLE,
		TRUEFALSE,
		GRT,
		GRT_EQ,
		LT,
		LT_EQ,
		EQ,
		NOT,
		ERROR,
		ADD,
		MINUS,
		NEGATIVE,
		MULTI,
		DIV,
		INT_DIV,
		GROUPING,
		FORALL,
		LET,
		SCOPE,
		INTEGER,
		REAL,
		BOOL
	}
	
	protected List<Logic> term_list
	protected String value
	Operation op
	
	new(Operation op){
		this.op = op		
		this.term_list = new ArrayList<Logic>
	}
	
	static def create_int(String number){
		return new SmtLogicInteger(number)
	}
	
	static def create_real(String number){
		return new SmtLogicReal(number)
	}
	
	static def create_bool(boolean value){
		var op = new Logic(Operation::BOOL)
		if(value){
			op.value = "true"	
		}else{
			op.value = "false"
		}		
		return op
	}
	
	static def create_variable(String name){
		return new SmtLogicVariable(name)
	}
	
	def getIntValue(){
		if(this.op == Operation::INTEGER){
			return new Integer(this.value)
		}
		throw new RuntimeException("Error")
	}
	
	def get_type(){
		return this.op
	}
	
	def copy_to_old_state(){
		var ns = new HashSet<String>
		this.copy_to_old_state(ns)
	}
	
	
	def Logic copy_to_old_state(Iterable<String> no_rename){
		switch this.get_type() {
			case Operation.FORALL:
			{
				var c_no_rename = new HashSet<String>
				c_no_rename.addAll(no_rename)
				for(c: (this.term_list.head as SmtLogicScope).pairs){
					c_no_rename.add(c.variable.variable)
				}
				Logic.forall(this.term_list.head as SmtLogicScope, this.term_list.get(1))
			}
			case Operation.VARIABLE:
			{
				if((this as SmtLogicVariable).variable.startsWith('t_')){
					new SmtLogicVariable((this as SmtLogicVariable).variable)
				}else{
					new SmtLogicVariable('t_'+ (this as SmtLogicVariable).variable)				
				}				
			}
			case Operation.TRUEFALSE:
			{
				this as SmtLogicTrueFalse
			}
			case Operation.INTEGER:
				this as SmtLogicInteger
			default: 
			{
				var c = new Logic(this.op)
				for(l: this.term_list){
					c.term_list.add(l.copy_to_old_state(no_rename))
				}
				c
			}
		}	
	}
	
	def get_parameters(){
		this.term_list
	}
		
	def String smt(){
		if(this.op == Operation::FORALL){
			var tmp = filterConstants(this.term_list.head as SmtLogicScope)
			if(tmp.varaible_count > 0){
				this.term_list.set(0, tmp)
			}else{
				return this.term_list.get(1).smt
			}
		} else if(this.op == Operation::EXISTS){
			var tmp = filterConstants(this.term_list.head as SmtLogicScope)
			if(tmp.varaible_count > 0){
				this.term_list.set(0, tmp)
			}else{
				return this.term_list.get(1).smt
			}
		}
		var str = '''
		(
		 «smt(this.op)» «FOR p:this.get_parameters SEPARATOR ' '»«IF p !== null»«p.smt()»«ELSE» --NULL-- «ENDIF»«ENDFOR»
		)
		'''
		
		str.replaceAll("\\(\\s*\\(","\\(\\(")
		str.replaceAll("\\s*\\(","\\(")
		str.replaceAll("\\)\\s*\\)","\\)\\)")
		str.replaceAll("\\s*\\)","\\)")
		return str
	}

	def static smt(Operation op){
		switch op{
			case Operation::AND: 'and'
			case Operation::OR: 'or'
			case Operation::ADD: '+'
			case Operation::MINUS: '-'
			case Operation::MULTI: '*'
			case Operation::DIV: '/'
			case Operation::EQ: '='
			case Operation::IMPLY: '=>'
			case Operation::GRT: '>'
			case Operation::GRT_EQ: '>='
			case Operation::LT: '<'
			case Operation::LT_EQ: '<='
			case Operation::NOT: 'not'
			case Operation::NEGATIVE: '-'
			case Operation::FORALL: 'forall'
			case Operation::EXISTS: 'exists'
			default: {
				'''not defined «op»'''
			}
		}
	}
	def static and(Logic l, Logic r){
		var t = new Logic(Operation.AND)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static or(Logic l, Logic r){
		var t = new Logic(Operation.OR)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static imply(Logic l, Logic r){
		var t = new Logic(Operation.IMPLY)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static grt(Logic l, Logic r){
		var t = new Logic(Operation.GRT)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static grt_eq(Logic l, Logic r){
		var t = new Logic(Operation.GRT_EQ)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static lt(Logic l, Logic r){
		var t = new Logic(Operation.LT)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static lt_eq(Logic l, Logic r){
		var t = new Logic(Operation.LT_EQ)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static equal(Logic l, Logic r){
		var t = new Logic(Operation.EQ)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static not(Logic l){
		var t = new Logic(Operation.NOT)
		t.term_list.add(l)
		t
	}
	
	def static add(Logic l, Logic r){
		var t = new Logic(Operation.ADD)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static minus(Logic l, Logic r){
		var t = new Logic(Operation.MINUS)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	
	def static div(Logic l, Logic r){
		var t = new Logic(Operation.DIV)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static int_div(Logic l, Logic r){
		var t = new Logic(Operation.INT_DIV)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	
	def static multi(Logic l, Logic r){
		var t = new Logic(Operation.MULTI)
		t.term_list.add(l)
		t.term_list.add(r)
		t
	}
	
	def static forall(SmtLogicScope scope_p, Logic expression){
		var scope = filterConstants(scope_p)
		var t = new Logic(Operation.FORALL)
		t.term_list.add(scope)
		t.term_list.add(expression)
		t
	}
	
	def static filterConstants(SmtLogicScope scope) {
		val ret_scope = new SmtLogicScope
		scope.pairs.forEach[
			if(!ConstantStore::containVariable(it.variable.variable)){
				ret_scope.add(it.variable.variable, it.type)
			}
		]
		return ret_scope
	}
	

	def static smt(TypeBase type){
		SmtCondition::smt_type(type)
	}
	
	def static negative(Logic logic) {
		var t = new Logic(Operation.NEGATIVE)
		t.term_list.add(logic)
		t
	}
	
	def static exists(SmtLogicScope scope_p, Logic expression) {
		var scope = filterConstants(scope_p)
		var t = new Logic(Operation.EXISTS)
		t.term_list.add(scope)
		t.term_list.add(expression)
		t
	}
	
	def static not_eq(Logic logic) {
		var t = new Logic(Operation.NOT)
		t.term_list.add(logic)
		t
	}
	
}




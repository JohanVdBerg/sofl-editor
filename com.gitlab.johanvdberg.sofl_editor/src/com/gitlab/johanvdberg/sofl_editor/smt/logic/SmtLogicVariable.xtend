package com.gitlab.johanvdberg.sofl_editor.smt.logic

class SmtLogicVariable extends Logic{
	
	public String variable
	
	new(String variable){
		super(Operation::VARIABLE)
		this.variable = variable
	}
	
	override smt(){
		this.variable
	}
}

package com.gitlab.johanvdberg.sofl_editor.smt.logic

import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.TreeMap
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpAnd
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpOr
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpImply
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNeq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpAdd
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpMinus
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpMult
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpIntDiv
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpGrt
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpLess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpGrtEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNeg
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpLessEq
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpScopeModify
import com.gitlab.johanvdberg.sofl_editor.types.ExpressionType
import com.gitlab.johanvdberg.sofl_editor.types.Natural0Type
import com.gitlab.johanvdberg.sofl_editor.types.IntegerType
import com.gitlab.johanvdberg.sofl_editor.types.NaturalType
import com.gitlab.johanvdberg.sofl_editor.types.RealType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.VariableDeclare
import com.gitlab.johanvdberg.sofl_editor.soflDsl.StateConditionsDefinition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflDslFactory
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflVariableName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNumber
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpBool
import com.gitlab.johanvdberg.sofl_editor.types.BooleanType
import com.gitlab.johanvdberg.sofl_editor.evaluate.Evaluate
import com.gitlab.johanvdberg.sofl_editor.smt.SmtNode
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpDiv
import org.eclipse.xtext.EcoreUtil2
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Constant
import com.gitlab.johanvdberg.sofl_editor.smt.file.ConstantStore
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExpNot

class SmtCondition {
	//public Expression condition
	Logic formula
	public Map<String, TypeBase> variables
	//public static SmtCondition True = new SmtCondition(true)
	
	def get_formula(){
		this.formula
	}
	
	protected new(Logic formula, Map<String, TypeBase> base_types){
		this.formula = formula		
		this.variables = new TreeMap<String, TypeBase>
		for(k: base_types.keySet()){
			this.variables.put(k, base_types.get(k))
		}
	}
	
	def static create(boolean exp){
		new SmtCondition(exp)
	}	
	
	def static create(SmtNode parent, boolean input, int port_index){
		new SmtStructureCondition(parent , input, port_index)
	}
	
	def static create(Logic formula, Map<String, TypeBase> base_types){
		new SmtCondition(formula, base_types)
	}
	
	def static create(String varaible, TypeBase base_types){
		val formula = Logic::create_variable(varaible)
		val scope = new HashMap<String, TypeBase> 
		scope.put(varaible, base_types)
		new SmtCondition(formula, scope)
	}
	
	protected new(){
		
	}
	
	protected new(boolean exp){
		var tmp = SoflDslFactory.eINSTANCE.createExpBool
		if(exp){
			tmp.is_true = true
			tmp.is_false = false
		}else{
			tmp.is_true = false
			tmp.is_false = true
		}
		this.variables = new TreeMap<String, TypeBase>
		this.formula = this.smt_formula(tmp)		
	}
	
	def static create(Expression exp){
		new SmtCondition(exp)	
	}
	
	def static create_equal(SoflVariableName name, Expression exp){
		new SmtCondition(exp)	
	}
	
	def static create(List<Expression> lst){
		lst.map[exp|new SmtCondition(exp)]	
	}
	
	
	protected new(Expression exp){
		this.variables = new TreeMap<String, TypeBase>
		this.formula = this.smt_formula(exp)
	}

	protected def Logic smt_formula(Expression exp){
		if(exp instanceof ExpAnd){
			Logic.and(smt_formula(exp.left), smt_formula(exp.right))		
		}else if(exp instanceof ExpOr){
			Logic.or(smt_formula(exp.left), smt_formula(exp.right))		
		}else if(exp instanceof ExpImply){
			Logic.imply(smt_formula(exp.left), smt_formula(exp.right))	
		}else if(exp instanceof ExpEq){
			Logic.equal(smt_formula(exp.left), smt_formula(exp.right))	
		}else if(exp instanceof ExpNeq){
			Logic.not(Logic.equal(smt_formula(exp.left), smt_formula(exp.right)))	
		}else if(exp instanceof ExpAdd){
			Logic.add(smt_formula(exp.left), smt_formula(exp.right))	
		}else if(exp instanceof ExpMinus){
			Logic.minus(smt_formula(exp.left), smt_formula(exp.right))	
		}else if(exp instanceof ExpMult){
			Logic.multi(smt_formula(exp.left), smt_formula(exp.right))	
		}else if(exp instanceof ExpIntDiv){
			Logic.div(smt_formula(exp.left), smt_formula(exp.right))	
		}else if(exp instanceof ExpDiv){
			Logic.div(smt_formula(exp.left), smt_formula(exp.right))
		}else if (exp instanceof ExpGrt){
			Logic.grt(smt_formula(exp.left), smt_formula(exp.right))
		}else if (exp instanceof ExpLess){
			Logic.lt(smt_formula(exp.left), smt_formula(exp.right))
		}else if (exp instanceof ExpGrtEq){
			Logic.grt_eq(smt_formula(exp.left), smt_formula(exp.right))
		}else if(exp instanceof ExpNeg){
			Logic.negative(smt_formula(exp.expression))
		}else if (exp instanceof ExpLessEq){
			Logic.lt_eq(smt_formula(exp.left), smt_formula(exp.right))
		}else if (exp instanceof ExpNot){
			Logic.not_eq(smt_formula(exp.expression))
		}else if(exp instanceof ExpScopeModify){
			if(exp.variable !== null){
				var variable = if(exp.is_un_mod){
					't_' + exp.variable.name
				}else{
					exp.variable.name
				}
				if(this.variables.containsKey(variable)){
					//throw new RuntimeException("Duplicate variable name")
				}else{
					var type = ExpressionType.getType(exp)
					this.variables.put(variable, type)
				}
				var context = EcoreUtil2::getContainerOfType(exp.variable, Constant)
				if(context !== null){
					var var_type = ExpressionType::getType(context.expression)
					var var_value = Evaluate::evaluate(context.expression)
					ConstantStore::add(exp.variable.name,  var_type, var_value)
					
				}
				return new SmtLogicVariable(variable)
			}else if(exp.is_grouping){
				var formula = smt_formula(exp.expression)
				if(formula.get_parameters.length >1){
					new LogicGrouping(formula)			
				}else{
					formula
				}
			}else if(exp.is_forall){
				//get the variables created
				var newVar = SmtCondition.getVaraiableSet(exp.bindings.declare.toList)
				var scope = new SmtLogicScope
				//keep a copy of existing duplicate names
				var store = new HashMap<String,TypeBase>
				for(tk: newVar.keySet){
					if(this.variables.containsKey(tk)){
						store.put(tk, this.variables.get(tk))
						this.variables.remove(tk)
					}
					scope.add(tk, newVar.get(tk))
				}
				//create formula end parse exp.expression
				var formula = Logic.forall(scope, this.smt_formula(exp.expression))
				//remove create variable from the free variable set
				for(tk: newVar.keySet){
					if(this.variables.containsKey(tk)){
						this.variables.remove(tk)
					}
				}
				//restore the stored variables.
				for(tk: store.keySet){
					this.variables.put(tk, this.variables.get(tk))
				}							
				return formula				
			}else{
				new SmtLogicError('''undefined 1 «exp»''')
			}			
		}else if(exp instanceof ExpNumber){
			//new SmtLogicInteger(Evaluate.evaluate(exp.number).toString)
			Evaluate.evaluate(exp.number)
		}else if(exp instanceof ExpBool){
			new SmtLogicTrueFalse(exp.is_true)
		}else{
			new SmtLogicError('''undefined 2 «exp»''')
		}
	}
	
	def SmtCondition to_previous_state(){		
		var st = this.get_formula.copy_to_old_state
		var new_varaibles = new HashMap<String, TypeBase>
		for(k: this.variables.keySet){
			if(k.startsWith('t_')){
				new_varaibles.put(k, this.variables.get(k))
			}else{
				new_varaibles.put('''t_«k»''', this.variables.get(k))
			}
		}
		new SmtCondition(st, new_varaibles)
	}
		
	def static smt_type(TypeBase type){
		if(type instanceof IntegerType){
			"Int"
		}else if(type instanceof Natural0Type){
			"Int"
		}else if(type instanceof NaturalType){
			"Int"
		}else if(type instanceof BooleanType){
			"Bool"
		}else if(type instanceof RealType){
			"Real"
		}else{
			"smt_type"
		}
	}
	
	def static getVaraiableSet(List<VariableDeclare> lst){
		val map = new HashMap<String, TypeBase>
		for(v: lst){
			val t = ExpressionType.getType(v.type)
			v.identifier.forEach[map.put(it.name, t)]
		}
		return map
	}			
	
	def static conjunction(Iterable<SmtCondition> exp_lst){
		if(exp_lst.empty){
			throw new RuntimeException('List of conditions are empty')
			//new SmtCondition(true)
		}else if(exp_lst.length == 1){
			exp_lst.head
		}else{
			var current = exp_lst.head
			var lst = exp_lst.tail
			var v_set = new TreeMap<String, TypeBase>
			v_set.putAll(current.variables)
			while(lst.length > 0){
				v_set.putAll(lst.head.variables)
				current = new SmtCondition(Logic.and(current.formula, lst.head.formula), v_set)
				lst = lst.tail
			}
			return current		
		}
	}
	
	def static getConditionlist(StateConditionsDefinition conditions){
		conditions.state_list.map[new PrePostPredicates(it)]
	}
	
	def static getCondition(StateConditionsDefinition conditions, int port_index, boolean input){
		if(conditions?.state_list === null){
			throw new RuntimeException('List of conditions are empty')
			//var tmp = SoflDslFactory.eINSTANCE.createExpBool
			//tmp.is_true = true
			//new SmtCondition(tmp)
		}else{
			val cond_list = conditions.state_list.filter[it|
				if(input){
					it.in_port_id == port_index	
				}else{
					it.out_port_id == port_index								
				}
			].map[it|
				if(input){
					it.pre.condition	
				}else{
					it.post.condition
				}
			]
			return disjunction(cond_list.toList.map[new SmtCondition(it)])		
		}
	}
	
	def static imply(SmtCondition l, SmtCondition r) {
		var v = new TreeMap<String, TypeBase>
		v.putAll(l.variables)
		v.putAll(r.variables)
		new SmtCondition(Logic.imply(l.get_formula, r.get_formula), v)
	}
	
	def static equal(SmtCondition l, SmtCondition r) {
		var v = new TreeMap<String, TypeBase>
		v.putAll(l.variables)
		v.putAll(r.variables)
		new SmtCondition(Logic.equal(l.get_formula, r.get_formula), v)
	}
	
	def static disjunction(java.util.List<SmtCondition> exp_lst) {
		if(exp_lst.length == 0){
			throw new RuntimeException('List of conditions are empty')
		}else{
			var current = exp_lst.head
			var lst = exp_lst.tail
			var v_set = new TreeMap<String, TypeBase>
			v_set.putAll(current.variables)
			while(lst.length > 0){
				v_set.putAll(lst.head.variables)
				current = new SmtCondition(Logic.or(current.formula, lst.head.formula), v_set)
				lst = lst.tail
			}
			return current
		}
	}
	
	def static forall(SmtCondition exp_scope, SmtCondition expression){
		var scope = new SmtLogicScope(exp_scope)
		SmtCondition::forall(scope, expression)
	}
	
	def static forall(SmtLogicScope scope, SmtCondition expression){
		var var_main = new TreeMap<String, TypeBase>
		//for(v: scope.pairs){			
		for(v_name: expression.variables.keySet){
			if(!scope.contain(v_name)){
				var_main.put(v_name, expression.variables.get(v_name))			
			}			
		}
		SmtCondition::create(Logic::forall(scope, expression.formula) , var_main)
	}
	
	override toString(){
		this.get_formula.smt
	}
	
	def static exists(SmtCondition exp_scope, SmtCondition expression) {		
		var scope = new SmtLogicScope(exp_scope)
		SmtCondition.exists(scope, exp_scope)
	}
	
	def static exists(SmtLogicScope scope, SmtCondition expression){
		var var_main = new TreeMap<String, TypeBase>
		for(v_name: expression.variables.keySet){
			if(!scope.contain(v_name)){
				var_main.put(v_name, expression.variables.get(v_name))			
			}			
		}
		SmtCondition::create(Logic::exists(scope, expression.formula) , var_main)
	}
	
	def static not(SmtCondition condition) {
		return SmtCondition.equal(condition, new SmtCondition(false))
	}
	
}
	
	
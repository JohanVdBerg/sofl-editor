package com.gitlab.johanvdberg.sofl_editor.smt.logic

class SmtLogicInteger extends Logic{
	
	int value
	
	new(int value){
		super(Operation.INTEGER)
		this.value = value
	}
	
	new(String value){
		super(Operation.INTEGER)
		this.value = new Double(value).intValue
	}
	
	override smt(){
		'''«this.value»'''
	}
	
}

package com.gitlab.johanvdberg.sofl_editor.smt.logic

class LogicGrouping extends Logic{

	new(Logic logic){
		super(Operation::GROUPING)
		this.term_list.add(logic)		
	}
	
	override smt(){
		'''«this.term_list.head.smt()»'''
	}
		
}

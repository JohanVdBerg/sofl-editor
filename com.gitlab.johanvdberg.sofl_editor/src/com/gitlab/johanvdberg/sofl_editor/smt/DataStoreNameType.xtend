package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NewVariable
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ExternalVariable
import com.gitlab.johanvdberg.sofl_editor.types.ExpressionType

class DataStoreNameType {
	public String name
	public TypeBase type
	
	new(NewVariable syntax_type){
		this.name = syntax_type.name.name
		this.type = ExpressionType::getType(syntax_type.type)
	}
	
	new(ExternalVariable syntax_type){
		if(syntax_type.variable_name !== null){
			this.name = syntax_type.variable_name.name
			this.type = ExpressionType::getType(syntax_type.variable_name)
		}else{
			this.name = syntax_type.ext_variable_name.name
			this.type = ExpressionType::getType(syntax_type.type)
		}	
	}
	
	new(String name, TypeBase n_type){
		this.name = name
		this.type = n_type
	}
	
	override toString(){
		'''«this.name»:«this.type»'''
	}
	
}
package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.semantics.Node.NodeType
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationCondition
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationPredicateTransform
import java.util.Map
import java.util.HashMap

class SmtConditionNode extends SmtStructureNode{
	protected new(ConditionStructure node, SmtModule parent){
		super(NodeType::Condition)		
		this.parent = parent		
		this.node_name = node.name
		var cond_list = node.out_list.map[SmtCondition::create(it.pred)]
		var in_cond = SmtCondition::disjunction(cond_list)
		this.add_port(#[new VariableType(node.in_variable.name)], in_cond, true, node)
		var count = 0
		for(out: node.out_list){
			val out_cond = SmtCondition::create(out.pred)
			this.add_port(#[new VariableType(out.varname.name)], out_cond, false, node)			
			/*this.io_conditions.add(IOPortRelation::create(
				0,
				count,
				in_cond,
				out_cond
			))*/
			count = count + 1			
		}
		this.connect_internal_ports
		this.flow_port_obligations.put('All output',new ProofObligationCondition(cond_list))
		val inport = this.get_inner_input_ports.head
		for(out:this.get_inner_output_ports){
			//List<SmtCondition> start_invariants,List<SmtCondition> store_invariants, SmtPortInNode start_port, SmtPortInNode end_port,  List<SmtCondition> end_invariants)
			this.transition_obligations.put(new SmtTransitionPorts(1 ,out.index + 1), ProofObligationPredicateTransform::create(
				parent.get_invariant, #[], inport, out, parent.get_invariant 
			))
		}
		
	}
	
	
	
	override computeProofObligations() {
		
	}
	
	
	override Map<String, SmtCondition> getInputPredicateList() {
		var out = new HashMap<String, SmtCondition>

		
		
		for (current : this.get_outer_destination_ports) {
			out.put('''In 1 Out all''', current.get_codition)
		}

		out
	}
}
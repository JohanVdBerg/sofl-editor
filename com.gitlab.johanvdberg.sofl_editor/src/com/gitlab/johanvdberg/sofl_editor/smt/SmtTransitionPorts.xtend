package com.gitlab.johanvdberg.sofl_editor.smt

class SmtTransitionPorts implements Comparable<SmtTransitionPorts>{
	
	int in_port
	int out_port
	
	new(int in_port, int out_port){
		this.in_port = in_port + 1 
		this.out_port = out_port + 1				
	}
	
	override toString(){
		if(this.out_port < 0){
			'''In port «this.in_port» Out port all'''
		}else{
			'''In port «this.in_port» Out port «this.out_port»'''
		}
	}
	
	override compareTo(SmtTransitionPorts o) {
		-(
			if(this.in_port < o.in_port){
				-1
			}else if(this.in_port == o.in_port){
				if(this.out_port < o.out_port){
					-1
				}else if(this.out_port == o.out_port){
					0
				}else{
					1
				}
			}else{
				1
			}
		)
	}
	
}
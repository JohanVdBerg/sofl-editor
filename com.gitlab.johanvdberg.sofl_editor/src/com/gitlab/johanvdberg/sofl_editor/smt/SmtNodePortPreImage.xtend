package com.gitlab.johanvdberg.sofl_editor.smt

import java.util.TreeSet
import org.eclipse.xtext.EcoreUtil2
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition

class SmtNodePortPreImage extends NotUsedSmtPortPreImage {

	
	new(SmtPort target_port){
		super(target_port)
				
		this.get_precondition_environment
	}
		
	def get_precondition_environment(){
		//port condition of port
		for(pre_port: (this.current_port as SmtPortInNode).get_connected_ports){
			var p_name = pre_port.parentName
			if(!this.pre_image.containsKey(p_name)){
				this.pre_image.put(p_name, new TreeSet<SmtPort>)
			}
			this.pre_image.get(p_name).add(pre_port)
			this.env_precondition_predicate_list.add(pre_port.get_condition(false))
		}
		//invariants that are applicable.
		//get the invariant of the containing module.
		var module = EcoreUtil2.getContainerOfType(this.current_port.syntax_port, SoflModule)
		if(module.invariant !== null){
			if(module.invariant.invariant.length ==0){
				this.env_precondition_predicate_list.add(SmtCondition::create(true))	
			}else{
				for(cond: module.invariant.invariant){
					var c = SmtCondition::create(cond)
					this.env_precondition_predicate_list.add(c)				
				}
			}
		}else{
			this.env_precondition_predicate_list.add(SmtCondition::create(true))	
		}
		//create conjunction of the results.
	}
	
}
package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.types.ConnectionType
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import java.util.List
import java.util.ArrayList
import java.util.HashMap
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import com.gitlab.johanvdberg.sofl_editor.types.ExpressionType
import com.gitlab.johanvdberg.sofl_editor.smt.logic.Logic

class SmtProcessNode extends SmtNode {
	
	public String decomposed_module_name
	private List<SmtCondition> read_only_stores_invaraints
	
	
	protected new(SoflProcess node, SmtModule parent){
		super(node.state)	
		this.read_only_stores_invaraints = new ArrayList<SmtCondition>	
		//this.out_port_obligations = new HashMap<String, ProofObligation>
		this.parent = parent
		//var n = new SmtNode(node.name)
		this.node_name = node.name
		var index = 1
		if(node.decom !== null){
			this.decomposed_module_name = node.decom.module.name
		}		
		for(port: node.in_ports.ports){
			var lst = (
				port.connections.map[new VariableType(it.name, new ConnectionType)] + 
				port.declare.map[it.identifier.map[v |new VariableType(v.name, getType(it.type))]].flatten)			
			var cond = SmtCondition.getCondition(node.state, index, true)
			this.add_port(lst.toList, cond, true, port)
			index = index + 1
		}
		index = 1
		for(port: node.out_ports.ports){
			var lst = (
				port.connections.map[new VariableType(it.name, new ConnectionType)] + 
				port.declare.map[it.identifier.map[v |new VariableType(v.name, getType(it.type))]].flatten)
			var cond = SmtCondition.getCondition(node.state, index, false)
			this.add_port(lst.toList, cond, false, port)
			index = index + 1
		}
		
		if(node.ext !== null){
			var base_type = new HashMap<String, TypeBase> 
			
			val wr_list = node.ext.variables.filter[it.wr].map[it.varariable_name.name].toSet
			val read_only_list = this.parent.data_store_names.filter[false && !wr_list.contains(it.name)].toList
			read_only_list.addAll(
				node.ext.variables.filter[it.rd].map[new DataStoreNameType(it.varariable_name.name, ExpressionType::getType(it.varariable_name))]
			)
			//val read_only_list = node.ext.variables.filter[it.rd].map[new DataStoreNameType(it.varariable_name.name, ExpressionType::getType(it.varariable_name))]
			for(el : read_only_list){
				base_type.put(el.name, el.type)
				base_type.put('''t_«el.name»''', el.type)
				var logic = Logic::create_variable(el.name)
				var lg = logic.copy_to_old_state(#[el.name])
				lg = Logic::equal(lg, logic)
				var scope = SmtCondition::create(logic, base_type).to_previous_state
				//this.read_only_stores_invaraints.add(SmtCondition::exists(scope, SmtCondition::create(lg, base_type)))
				this.read_only_stores_invaraints.add(SmtCondition::create(lg, base_type))
			}
		}
	}
	
	
	override get_store_invariant() {
		read_only_stores_invaraints
	}
	
}
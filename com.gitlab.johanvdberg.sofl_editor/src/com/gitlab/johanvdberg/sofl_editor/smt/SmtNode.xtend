package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.semantics.Node.NodeType
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligation
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationFlowPredicate
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationPredicateTransform
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.StateConditionsDefinition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure
import com.gitlab.johanvdberg.sofl_editor.types.ExpressionType
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.EObject

abstract class SmtNode {
	protected ArrayList<SmtPortInNode> in_ports
	protected ArrayList<SmtPortInNode> out_ports
	public Map<String, ProofObligation> flow_port_obligations
	public Map<SmtTransitionPorts, ProofObligation> transition_obligations
	protected String node_name
	public SmtModule parent
	public List<IOPortRelation> io_conditions
	public NodeType nodeType

	protected new() {
		this.nodeType = NodeType::Other
		this.io_conditions = new ArrayList<IOPortRelation>
		this.flow_port_obligations = new HashMap<String, ProofObligation>
		this.transition_obligations = new HashMap<SmtTransitionPorts, ProofObligation>
		this.in_ports = new ArrayList<SmtPortInNode>
		this.out_ports = new ArrayList<SmtPortInNode>

	}

	protected new(NodeType nodeType) {
		this.nodeType = nodeType
		this.io_conditions = new ArrayList<IOPortRelation>
		this.flow_port_obligations = new HashMap<String, ProofObligation>
		this.transition_obligations = new HashMap<SmtTransitionPorts, ProofObligation>
		this.in_ports = new ArrayList<SmtPortInNode>
		this.out_ports = new ArrayList<SmtPortInNode>
	}

	protected new(StateConditionsDefinition cond) {
		this.nodeType = NodeType::Other
		this.io_conditions = new ArrayList<IOPortRelation>
		this.flow_port_obligations = new HashMap<String, ProofObligation>
		this.transition_obligations = new HashMap<SmtTransitionPorts, ProofObligation>
		this.in_ports = new ArrayList<SmtPortInNode>
		this.out_ports = new ArrayList<SmtPortInNode>

		for (current : cond.state_list) {
			this.io_conditions.add(IOPortRelation::create(
				current.in_port_id - 1,
				current.out_port_id - 1,
				current.pre.condition,
				current.post.condition
			))
		}
	}

	def getNodeType() {
		this.nodeType
	}

	def List<SmtCondition> get_store_invariant() {
		#[SmtCondition::create(true)]
	}

	def static getType(SoflType type) {
		ExpressionType.getType(type)
	}

	def get_name() {
		return this.node_name
	}

	def static create(SoflBehaviour node, SmtModule parent) {
		new SmtBehviorNode(node, parent)
	}

	def static create(BroadcastStructure node, SmtModule parent) {
		new SmtStructureNode(node, parent)
	}

	def static create(SoflProcess node, SmtModule parent) {
		new SmtProcessNode(node, parent)
	}

	def static create(ConditionStructure node, SmtModule parent) {
		new SmtConditionNode(node, parent)
	}

	def static create(MergeStructure node, SmtModule parent) {
		new SmtStructureNode(node, parent)
	}

	def static create(UnMergeStructure node, SmtModule parent) {
		new SmtStructureNode(node, parent)
	}

	def void computeProofObligations() {
		// compute obligations of the output ports	
		for (start_port : this.get_inner_input_ports()) {
			val start_invariant = this.get_port_invariant(start_port)
			val store_invariant = if (this instanceof SmtProcessNode) {
					(this as SmtProcessNode).get_store_invariant()
				} else {
					#[]
				}
			for (end_port : this.get_inner_output_ports()) {
				val end_invariant = this.get_port_invariant(end_port)
				/*if (this instanceof SmtProcessNode) {
					for (t : (this as SmtProcessNode).get_store_invariant()) {
						end_invariant.add(t)
					}
				}*/
				var obligation = ProofObligationPredicateTransform::create(start_invariant, store_invariant, start_port,
					end_port, end_invariant)
				this.transition_obligations.put(new SmtTransitionPorts(start_port.index, end_port.index), obligation)
			}
		}
		for (port : this.get_outer_destination_ports()) {
			// write the test for the environment
			val temp = port.connected_ports.map [
				it.parent.getInputPredicateList().values
			].flatten.map[it.to_previous_state].toList
			val env = if (temp.length > 0) {
					#[
						port.connected_ports.map[it.get_condition(true)],
						#[SmtCondition.disjunction(temp)]
					].flatten.toList
				} else {
					port.connected_ports.map[it.get_condition(true)].toList
				}
			for (out_port : this.get_inner_output_ports()) {
				var port_condition = port.get_condition(out_port.getIndex())
				var current = new ProofObligationFlowPredicate(env, port_condition, port.index, out_port.index)
				this.flow_port_obligations.put(current.obligation_name, current)
			}
			var port_condition = port.get_condition(true)
			var current = new ProofObligationFlowPredicate(env, port_condition, port.index)
			this.flow_port_obligations.put(current.obligation_name, current)
		}
	}

	def getName() {
		this.node_name
	}

	def is_decomposed() {
		false
	}

	def add_port(List<VariableType> varaible_list, SmtCondition condition, boolean input, EObject syntax_port) {
		val ss = varaible_list.toSet
		if (ss.length != varaible_list.length) {
			throw new RuntimeException('''A duplicate variable name exist «varaible_list»''')
		}
		if (input) {
			var p = new SmtPortInNode(this, this.in_ports.length, varaible_list, condition, syntax_port)
			p.set_as_destination(true)
			this.in_ports.add(p)
		} else {
			var p = new SmtPortInNode(this, this.out_ports.length, varaible_list, condition, syntax_port)
			p.set_as_destination(false)
			this.out_ports.add(p)
		}
	}

	def get_inner_input_ports() {
		this.in_ports
	}

	def get_inner_output_ports() {
		this.out_ports
	}

	def get_outer_source_ports() {
		this.out_ports
	}

	def get_outer_destination_ports() {
		this.in_ports
	}

	def get_port_invariant(SmtPortInNode port) {
		if (this.get_inner_input_ports.contains(port) && port.is_use_init_condidtion) {
			var lst = new ArrayList<SmtCondition>
			lst.addAll(this.parent.get_init_invariant)
			lst.addAll(this.parent.get_invariant)
			lst
		} else {
			this.parent.get_invariant
		}
	}

	def SmtCondition getDestinationPredicate(int input, int output) {
		var lst = this.io_conditions.filter[it.in_port == input && it.out_port == output]
		if (lst.length != 1) {
			var str = '''Error «lst.length»  «lst»'''
			throw new RuntimeException(str)
		}
		lst.head.pre_condition
	}

	def SmtCondition getSourcePredicate(int input, int output) {
		var lst = this.io_conditions.filter[it.in_port == input && it.out_port == output]
		if (lst.length != 1) {
			var str = '''Error «lst.length»  «lst»'''
			throw new RuntimeException(str)
		}
		lst.head.post_condition
	}

	def Map<String, SmtCondition> getInputPredicateList() {
		var out = new HashMap<String, SmtCondition>

		for (current : this.io_conditions) {
			out.put('''In «current.in_port» Out «current.out_port»''', current.pre_condition)
		}

		out
	}

	def Map<String, SmtCondition> getOutputPredicateList() {
		var out = new HashMap<String, SmtCondition>

		for (current : this.io_conditions) {
			out.put('''In «current.in_port» Out «current.out_port»''', current.post_condition)
		}

		out
	}

	def static getCondition(SoflProcess node, int port_index, boolean input) {
		var lst = if (input) {
				node.state.state_list.filter[it|it.in_port_id == port_index].map[it|it.pre.condition]
			} else {
				node.state.state_list.filter[it|it.out_port_id == port_index].map[it|it.post.condition]
			} // .filter[it !== null]
		var current = SmtCondition::conjunction(lst.toList.map[SmtCondition::create(it)])
		current
	}

	def static getCondition(SoflBehaviour node, int port_index, boolean input) {
		var lst = if (input) {
				node.state.state_list.filter[it|it.in_port_id == port_index].map[it|it.pre.condition]
			} else {
				node.state.state_list.filter[it|it.out_port_id == port_index].map[it|it.post.condition]
			} // .filter[it !== null]
		var current = SmtCondition::conjunction(lst.toList.map[SmtCondition::create(it)])
		current
	}

	def get_parent() {
		this.parent
	}

	def boolean have_parent() {
		this.parent !== null
	}

	override toString() {
		this.node_name
	}

}

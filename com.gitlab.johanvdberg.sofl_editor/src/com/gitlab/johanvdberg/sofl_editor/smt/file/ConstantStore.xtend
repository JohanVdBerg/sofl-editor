package com.gitlab.johanvdberg.sofl_editor.smt.file

import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase
import java.util.Set
import java.util.TreeSet

class ConstantStore {
	public static ConstantStore store
	Set<StoreEntry> entires
	
	
	protected new(){
		this.entires = new TreeSet<StoreEntry>
	}
	
	protected def _getTypeOf(String name){
		var cur = this.getEntryOf(name)
		cur.type
	}
	
	protected def _getValueOf(String name){
		var cur = this.getEntryOf(name)
		cur.value
	}
	
	protected def _containVariable(String name){
		this.entires.findFirst[it.name == name] !== null
	}
	
	private def getEntryOf(String name){
		var tmp = this.entires.filter[it.getName() == name].toList
		if(tmp.length != 1){
			throw new RuntimeException("error")
		}
		return tmp.head
	}
	
	protected def  _add(String name, TypeBase type, SmtCondition value) {
		this.entires.add(new StoreEntry(name, type, value))
	}
	
	private def _smt(Set<String> variables){
		var ret = ""
		for(name:variables){
			ret = '''
			«ret»
			(assert (= «name» «this._getValueOf(name)»))
			'''
		}
		ret
	}
	
	static def init(){
		if(store === null){
			store = new ConstantStore
		}
		
	}
	
	static def add(String name, TypeBase type, SmtCondition value){
		init()	
		store._add(name, type, value)
	}
	
	
	
	static def getTypeOf(String name){
		init()
		store._getTypeOf(name)
	}
	
	static def getValueOf(String name){
		init()
		store._getValueOf(name)
	}
	
	static def containVariable(String name){
		init()
		store._containVariable(name)
	}
	
	def static smt(Set<String> strings) {
		if(store === null){
			""
		}else{
			store._smt(strings)
		}
	}
	
	def static Iterable<StoreEntry> constants(){
		init()
		store.entires
	}
	
}


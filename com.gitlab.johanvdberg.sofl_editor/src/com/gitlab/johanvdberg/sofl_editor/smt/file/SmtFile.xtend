package com.gitlab.johanvdberg.sofl_editor.smt.file

import org.eclipse.xtext.generator.IFileSystemAccess2

class SmtFile {
	String filename
	public ProofType proofType
	String smtFileContent
	String filelocation
	
	enum ProofType{
		StartState,
		EndState,
		Constrain
	}
	
	new(String filelocation, String filename, ProofType proofType, String smtFileContent){
		this.filename = filename
		this.filelocation = filelocation
		this.proofType = proofType
		this.smtFileContent = smtFileContent
	}
	
	
	def getFileName(){
		this.filename
	}
	
	def write_file(IFileSystemAccess2 fsa){
		fsa.generateFile('''«this.filelocation»/«this.fileName»''', this.smtFileContent)
	}
	
	def write_markdown(){
		'''[Filename](«filename») «filename» contents:
		    
		    «this.smtFileContent»
		    
		'''
	}
}
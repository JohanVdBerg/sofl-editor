package com.gitlab.johanvdberg.sofl_editor.smt.file

import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.types.TypeBase

class StoreEntry implements Comparable<StoreEntry> {
	String name
	TypeBase type
	SmtCondition value
	
	new(String name, TypeBase type, SmtCondition value){
		this.name = name
		this.type = type
		this.value = value
	}
	
	def getName(){
		name
	}
	
	def getType(){
		type
	}
	
	def getValue(){
		value
	}
	
	override compareTo(StoreEntry o) {
		var p1_c = this.name.compareTo(o.name)
		if(p1_c < 0){
			return p1_c
		}else if(p1_c > 0){
			return p1_c
		}
		return 0
	}
	
}
package com.gitlab.johanvdberg.sofl_editor.smt

import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligation
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationPredicateTransform
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationRefinementDown
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationRefinementUp
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationSatisfiable
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.types.VariableType
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationFlowPredicate
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationImply
import com.gitlab.johanvdberg.sofl_editor.smt.logic.ProofObligationInvariantImply

class SmtBehviorNode extends SmtNode {
	public Map<String, ProofObligation> refinement_up
	public Map<String, ProofObligation> refinement_down
	public ProofObligation invariant_condition
	// public Map<String, ProofObligation> out_flow
	public List<SmtConditionNode> condition_nodes

	protected new(SoflBehaviour node, SmtModule parent) {
		super(node.state)
		this.flow_port_obligations = null
		this.refinement_up = new HashMap<String, ProofObligation>
		this.refinement_down = new HashMap<String, ProofObligation>
		this.condition_nodes = new ArrayList<SmtConditionNode>
		this.parent = parent
		this.node_name = node.name
		this.in_ports = new ArrayList<SmtPortInNode>
		// this.out_ports = new ArrayList<SmtPortInNode>
		val oports = if (node?.grouping?.out_grouping?.ports !== null) {
				SmtPort.create(this, node.out_ports, node.adding.out_added, node.grouping.out_grouping,
					SmtCondition.getConditionlist(node.state), true)
			} else if (node.state !== null) {
				SmtPort.create(this, node.out_ports, SmtCondition.getConditionlist(node.state), true)
			} else {
				val l = VariableType::create(node.out_ports)
				var out = new ArrayList<SmtPort>
				for (port_index : 0 ..< l.length) {
					// get the conditions from the condition of the CDFD
					var p = new SmtPortInNode(this, port_index, l.get(port_index), SmtCondition::create(true),
						node.in_ports)
					p.set_as_destination(true)
					out.add(p)
				}
				out
			}
		val iports = if (node?.grouping?.in_grouping?.ports !== null) {
				SmtPort.create(this, node.in_ports, node.adding.in_added, node.grouping.in_grouping,
					SmtCondition.getConditionlist(node.state), false)
			} else if (node.state !== null) {
				SmtPort.create(this, node.in_ports, SmtCondition.getConditionlist(node.state), false)
			} else {
				val l = VariableType::create(node.in_ports) 
				var out = new ArrayList<SmtPort>
				for (port_index : 0 ..< l.length) {
					// get the conditions from the condition of the CDFD
					var p = new SmtPortInNode(this, port_index, l.get(port_index), SmtCondition::create(true),
						node.out_ports)
					p.set_as_destination(false)
					out.add(p)
				}
				out
			}

		for (port : oports) {
			this.out_ports.add(port as SmtPortInNode)
		}
		for (port : iports) {
			this.in_ports.add(port as SmtPortInNode)
		}

		// add condition nodesimply_condition
		if (node.conditions !== null) {
			for (c_node : node.conditions) {
				this.condition_nodes.add(new SmtConditionNode(c_node, this.parent))
			}
		}
		this.flow_port_obligations = null
	}

	override computeProofObligations() {
		// if refinement relation exist
		System.out.println('''Compute prove obligation of «this.name»''')
		if (this.parent.have_parent) {
			var refined_process = this.parent.get_process_being_refined()
			// var o_r_port_list = refined_process.out_ports
			var upper_invariant = new ArrayList<SmtCondition>
			var parent_parent = this.parent.get_parent

			upper_invariant.addAll(parent_parent.get_invariant)

			var p_out_port = 0
			for (oport_list :this.get_refine_port_grouping(false)) {
				var refined_port = refined_process.out_ports.get(p_out_port)
				this.refinement_up.put(
					'''«refined_port.index+1»''',
					new ProofObligationRefinementUp(this.parent.get_invariant, oport_list.map[it.condition], refined_port,
						upper_invariant)
				)
				p_out_port=p_out_port+1
			}

			// upper_invariant.addAll(parent_parent.initInvariants)
			var p_in_port = 0
			for (iport_list : this.get_refine_port_grouping(true)) {
				var refined_port = refined_process.in_ports.get(p_in_port)

				this.refinement_down.put(
					'''«refined_port.index+1»''',
					new ProofObligationRefinementDown(upper_invariant, refined_port, iport_list.map[it.condition],
						this.parent.get_invariant)
				)
				p_in_port = p_in_port + 1
			}

		}
		// compute obligations of the output ports	
		for (start_port : this.in_ports) {
			for (end_port : this.out_ports) {
				var obligation = ProofObligationPredicateTransform::create(get_port_invariant(start_port), #[],
					start_port, end_port, get_port_invariant(end_port))
				this.transition_obligations.put(new SmtTransitionPorts(start_port.index, end_port.index), obligation)
			}
		}
		// test the if the invariant is  satisfy; then the initilisation is possible
		if (this.parent.initInvaraints.length > 0 && this.parent.invariant_list.length > 0) {
			this.invariant_condition = new ProofObligationInvariantImply(this.parent.initInvaraints,
				this.parent.invariant_list, "Verify invariant condition")
		}
	}

	def get_refined_process() {
		if (!this.parent.have_parent) {
			throw new RuntimeException
		}
		val found = this.parent.parent.getAllProcesses().findFirst[it.decomposed_module_name == this.parent.name]
		if (found === null) {
			throw new RuntimeException('''''')
		}
		found
	}

	def get_refine_port_grouping(boolean internal_input_port) {
		val refine_port = new ArrayList<ArrayList<SmtPortInNode>>
		val proc = this.get_refined_process
		val proc_ports = if (internal_input_port) {
				proc.get_inner_input_ports
			} else {
				proc.get_inner_output_ports
			}
		val cdfd_ports = if (internal_input_port) {
				this.get_inner_input_ports
			} else {
				this.get_inner_output_ports
			}
		var c_port_index = 0

		for (port : proc_ports) {
			val tmp = new ArrayList<SmtPortInNode>
			val cond = cdfd_ports.length > c_port_index 
			while ((cdfd_ports.length > c_port_index) && SmtPort::constain_varaibles(port, cdfd_ports.get(c_port_index))){
				tmp.add(cdfd_ports.get(c_port_index))
				c_port_index = c_port_index + 1
			}
			if(tmp.length == 0){
				throw new RuntimeException
			}
			refine_port.add(tmp)
		}
		refine_port
	}

	override have_parent() {
		this.parent.have_parent
	}

	override get_outer_source_ports() {
		this.in_ports
	}

	override get_outer_destination_ports() {
		this.out_ports
	}

	override get_port_invariant(SmtPortInNode port) {
		if (this.get_inner_input_ports.contains(port)) {
			var lst = new ArrayList<SmtCondition>
			lst.addAll(this.parent.get_init_invariant)
			lst.addAll(this.parent.get_invariant)
			lst
		} else {
			this.parent.get_invariant
		}
	}

	def get_refinement_obligation() {
		if (this.parent.have_parent) {
			var relation = new HashMap<String, ProofObligation>

			for (ik : this.refinement_down.keySet) {
				relation.put('''Refine Down «ik»''', this.refinement_down.get(ik))
			}

			for (ok : this.refinement_up.keySet) {
				relation.put('''Refine Up «ok»''', this.refinement_up.get(ok))
			}

			relation
		} else {
			throw new RuntimeException("Must have parent to be allowed to call this method")
		}
	}

	override SmtCondition getDestinationPredicate(int input, int output) {
		super.getSourcePredicate(output, input)
	}

	override SmtCondition getSourcePredicate(int input, int output) {
		super.getDestinationPredicate(output, input)
	}

}

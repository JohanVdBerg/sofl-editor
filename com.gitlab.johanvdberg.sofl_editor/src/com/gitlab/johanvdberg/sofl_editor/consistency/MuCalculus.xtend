package com.gitlab.johanvdberg.sofl_editor.consistency


import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.semantics.Module

class CdfdIo {
	public String in
	public String out
	public String formula
}

class MuCalculus {

	protected Module module

	new(Module module) {
		this.module = module

	}

	def allPathGenerateData(){
		var out_list = new ArrayList<CdfdIo>
		for (cdfd_in : this.module.in_port_list) {
			var tmp = new CdfdIo
			tmp.in = cdfd_in.name
			tmp.out = 'all'
			tmp.formula = '''<true*.cdfd_input(«tmp.in»)>[cdfd_consume](exists cip:PortId.<cdfd_output(cip)>true)'''
				out_list.add(tmp)
		}
		return out_list			
	}
	
	// test if multiple output after generated
	def eachInportMultipleOutput() {
		var out_list = new ArrayList<CdfdIo>
		for (cdfd_in : this.module.in_port_list) {
			var tmp = new CdfdIo
			tmp.in = cdfd_in.name
			tmp.out = 'all'
			tmp.formula = '''
			forall cip:PortId.val(cip == «tmp.in») && <true*.cdfd_input(«tmp.in»)>true =>  
				exists cop:PortId.exists cop2:PortId.val(cop != cop2) && 
					<true*.cdfd_output(cop).true*.cdfd_output(cop2)>true'''
			out_list.add(tmp)
		}
		return out_list
	}

	// make sure each input of the cdfd will results in an execution of a node
	def eachInportProcessExecute() {
		var out_list = new ArrayList<Pair<String, String>>
		for (in_port : this.module.in_port_list.map[it|it.name]) {
			out_list.add(new Pair(
				in_port,'''
				exists nid:NodeId.
					<true*.cdfd_input(«in_port»).true*.fire(nid)>true
				'''
			))
		}
		return out_list
	}

	def eachEachProcessExecute() {
		var out_list = new ArrayList<Pair<String, String>>
		for (node : this.module.node_list.values.map[it.mcrl2_name].filter[it !== "NIp_Init"]) {
			out_list.add(new Pair(
				node,'''
				exists  pid:PortId.
					<true*.cdfd_input(pid).true*.fire(«node»)>true
				'''
			)) 
		}
		return out_list
	}
	
	def moreOncePortConsume() {
		var out_list = new ArrayList<Pair<String, String>>
		for (node : this.module.node_list.values.map[it.mcrl2_name].filter[it !== "NIp_Init"]) {
			out_list.add(new Pair(
				node,'''
				exists pid1,pid2:PortId.val(pid1 != pid2) && exists pod1,pod2:PortId.
					<true*.env_new_start> (<(!env_new_start)*.fire_ports(«node»,pid1, pod1)>true && <(!env_new_start)*.fire_ports(«node»,pid2, pod2)>true)
				'''
			))
		}
		return out_list
	}
	
	
	// test if a process can execute an infinite number of times
	def processFireInfinite() {
		var out_list = new ArrayList<Pair<String, String>>
		for (node_name : this.module.node_list.values.map[it.mcrl2_name].filter[it !== "NIp_Init"]) {
			out_list.add(new Pair(node_name, '''
			nu X.<true*.fire(«node_name»)>X
			'''))
		}
		return out_list
	}

	// test that for each input put port and 
	// output port will generate data
	def cdfd_in_generate() {
		var out_list = new ArrayList<CdfdIo>
		for (cdfd_in : this.module.in_port_list) {
			var tmp = new CdfdIo
			tmp.in = cdfd_in.name
			tmp.formula = '''
			exists cip:PortId.<true*.cdfd_input(«tmp.in»).true*.cdfd_output(cip)>true
			'''
			out_list.add(tmp)
		}
		return out_list
	}

	// determine the io relations of a cdfd	
	def cdfd_io_relation() {
		var out_list = new ArrayList<CdfdIo>
		for (cdfd_in : this.module.in_port_list) {
			for (cdfd_out : this.module.out_port_list) {
				var tmp = new CdfdIo
				tmp.in = cdfd_in.name
				tmp.out = cdfd_out.name
				tmp.formula = '''
					<true*.cdfd_input(«tmp.in»).true*.cdfd_output(«tmp.out»)>true
					'''
				out_list.add(tmp)
			}
		}
		return out_list
	}
	
	// determine if multiple outputs can be generated
	def cdfd_multi_output(){
		var out_list = new ArrayList<CdfdIo>
		for (cdfd_in : this.module.in_port_list) {
			for (cdfd_out : this.module.out_port_list) {
				var tmp = new CdfdIo
				tmp.in = cdfd_in.name
				tmp.out = cdfd_out.name
				tmp.formula = '''
					exists cop:PortId.<true*.cdfd_output(«tmp.out»).true*.cdfd_output(cop)>true
				'''
				out_list.add(tmp)
			}
		}
		return out_list		
	}

}
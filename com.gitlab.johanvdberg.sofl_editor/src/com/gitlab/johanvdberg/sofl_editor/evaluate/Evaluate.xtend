package com.gitlab.johanvdberg.sofl_editor.evaluate

import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtLogicInteger
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflNumber
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtLogicReal

class Evaluate {

	def static evaluate(SoflNumber parsed) {
		var str = ''
		if (parsed.is_negative) {
			str = '-'
		}
		str = str + parsed.number
		if (parsed.fraction !== null) {
			str = str + '.' + parsed.fraction
		}

		if (parsed.fraction !== null) {
			new SmtLogicReal(str)
		} else {
			new SmtLogicInteger(str)
		}
	}

	def static SmtCondition evaluate(Expression exp) {
		return SmtCondition::create(exp)
	}
		/*
		if (exp === null) {
			throw new RuntimeException('error')
		}
		if (exp instanceof ExpOr) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::or(left, right);
		} else if (exp instanceof ExpAnd) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::and(left, right);
		} else if (exp instanceof ExpImply) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::imply(left, right);
		} else if (exp instanceof ExpEquiv) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::and(Logic::imply(left, right), Logic::imply(right, left));
		} else if (exp instanceof ExpEq) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::equal(left, right)
		} else if (exp instanceof ExpNeq) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::not(Logic::equal(left, right))
		} else if (exp instanceof ExpInSet) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			throw new Exception("Set operations not implemented")
		} else if (exp instanceof ExpNotIn) {
			// var left = evaluate(exp.left)
			// var right = evaluate(exp.right)
			throw new Exception("Set operations not implemented")
		} else if (exp instanceof ExpLess) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::lt(left, right)
		} else if (exp instanceof ExpGrt) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::grt(left, right)
		} else if (exp instanceof ExpLessEq) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::lt_eq(left, right)
		} else if (exp instanceof ExpGrtEq) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::grt_eq(left, right)
		} else if (exp instanceof ExpAdd) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::add(left, right)
		} else if (exp instanceof ExpMinus) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::minus(left, right)
		} else if (exp instanceof ExpMult) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::multi(left, right)
		} else if (exp instanceof ExpPower) {
			// var left = evaluate(exp.left)
			// var right = evaluate(exp.right)
			throw new Exception("Set operations not implemented")
		} else if (exp instanceof ExpDiv) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::div(left, right)
		} else if (exp instanceof ExpIntDiv) {
			var left = evaluate(exp.left)
			var right = evaluate(exp.right)
			return Logic::div(left, right)
		} else if (exp instanceof ExpNot) {
			var type = evaluate(exp.expression)
			return Logic::not(type)
		} else if (exp instanceof ExpString) {
			// var str = evaluate(exp.string)
			throw new Exception("Set operations not implemented")
		} else if (exp instanceof ExpBool) {
			return Logic::create_bool(exp.is_true)
		} else if (exp instanceof ExpEnum) {
			throw new Exception("Set operations not implemented")
		} else if (exp instanceof ExpNumber) {
			return evaluate(exp.number)
		} else if (exp instanceof ExpNeg) {
			var type = evaluate(exp.expression)
			return Logic::negative(type)
		} else if (exp instanceof ExpScopeModify) {
			if (exp.is_curly) {
				if (exp.map_seq !== null) {
					throw new Exception("Set operations not implemented")
				} else if (exp.set_compress !== null) {
					throw new Exception("Set operations not implemented")
				} else if (exp.expression_seq !== null) {
					throw new Exception("Set operations not implemented")
				}
			} else if (exp.is_square) {
				throw new Exception("Set operations not implemented")
			} else if (exp.is_case) {
				throw new Exception("Set operations not implemented")
			} else if (exp.is_let) {
				throw new Exception("Set operations not implemented")
			} else if (exp.is_if) {
				throw new Exception("Set operations not implemented")
			} else if (exp.variable !== null) {
				throw new Exception("Set operations not implemented")
			} else if (exp.is_grouping) {
				throw new Exception("Set operations not implemented")
			} else if (exp.is_exists) {
				throw new Exception("Set operations not implemented")
			} else if (exp.is_forall) {
				throw new Exception("Set operations not implemented")
			} else if (exp.function_apply !== null) {
				throw new Exception("Set operations not implemented")
			}
		}
		throw new Exception("Set operations not implemented")
	}*/
}


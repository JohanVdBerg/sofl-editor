package com.gitlab.johanvdberg.sofl_editor.generator

import com.gitlab.johanvdberg.sofl_editor.semantics.Module
import java.util.ArrayList
import java.util.List
import org.eclipse.xtext.generator.IFileSystemAccess2
import com.gitlab.johanvdberg.sofl_editor.semantics.Port
import com.gitlab.johanvdberg.sofl_editor.semantics.Node
import com.gitlab.johanvdberg.sofl_editor.semantics.Flow
import com.gitlab.johanvdberg.sofl_editor.semantics.NodeInterface

/**
 * Used to generate a process algebra description of a SOFL data FlowId diagram
 */
class ProcAlgGenerate {
	//List<String> node_ids
	List<String> stores_ids
	IFileSystemAccess2 fsa
	int max_port_count
	Module module
	String base_location

	new(Module module, String base_location, IFileSystemAccess2 fsa) {
		//this.node_ids = new ArrayList<String>
		this.stores_ids = new ArrayList<String>
		this.max_port_count = 0
		this.base_location = base_location
		this.fsa = fsa
		this.module = module		
		add_store_sort(module)
		this.stores_ids.addAll(module.data_stores.map[String e|return this.store_name(e)])
		this.max_port_count = Math.max(this.max_port_count, module.max_port_count)
	}

	def file_name(){
		return this.base_location + this.module.name + '.mcrl2'
	}
	
	def write() {
		var specification = this.get_sort_block()

		specification = specification + this.get_maps_block()
		specification = specification + this.get_var_block()
		specification = specification + this.get_eqn_block()
		specification = specification + this.get_act_block()
		specification = specification + this.get_proc_block()

		specification = specification + this.get_init_block()
		this.fsa.generateFile(file_name(), specification)
	}

	def file_name_environment(){
		return this.base_location + this.module.name + '.environment.mcrl2'
	}
	
	
	def file_name_node(){
		return this.base_location + this.module.name + '_node.mcrl2'
	}

	// _consume
	def get_init_block() {
		return '''
			init 
				allow({
						error,
						phase_process,
						%phase_control,
						store_check,
						start_fire,
						selected_port_generate,
						fire,
						fire_ports,
						flow_action,
						flow_query,
						flow_consume,
						skip,
						cdfd_input,
						cdfd_output,
						done_fire,
						cdfd_start,
						cdfd_consume,
						env_new_phase,
						%env_next_env_end,
						%env_next,
						env_new_start,
						env_wait_to_start,
						env_move_to_end,
						env_start_done,
						store_update,
						wait_env_store_check,
						%node_execute,
						store_update_to_process,
						node_select_in_port,
						node_done_access_update,
						error_r_w,
						exec_phase_process
					},  
				comm({
						env_store_check     |store_store_check   |node_store_check  -> store_check,
						env_cdfd_start      |cdfd_cdfd_start     -> cdfd_start,
						env_flow            |node_flow         -> flow_action,
						env_flow_consume    |node_flow_consume -> flow_consume,
						env_flow_query      |node_flow_query   -> flow_query,
						env_node_start      |node_execute        -> start_fire,
						env_store_update    |node_store_update   -> store_update,
						env_node_end        |node_end            -> done_fire
						%env_cdfd_consume    |cdfd_cdfd_consume   -> cdfd_consume
					}, 
				«FOR nid : this.module.node_list.values.map[node_name(it)] SEPARATOR '||'»Node(«nid»)«ENDFOR»
				|| EnvironmentFirst || EnvironmentFlowIds({}) || CdfdIO || EnvironmentStore(init_store, init_store)
			));
		'''
	}


	def get_sort_block() {
		return '''
			% The data flow id's used
			sort FlowId       = struct «FOR fw : this.module.flow_list.map[it|it.flow_name] SEPARATOR ' | '»«fw»«ENDFOR»;
			% The node id's of the nodes in the diagram
			sort NodeId       = struct cdfd_node | «node_name(this.module.get_node_name)»«FOR ndid : this.module.node_list.values.map[node_name(it)] BEFORE ' | ' SEPARATOR ' | '»«ndid»«ENDFOR»;
			% Indexes used to identify ports
			sort PortId       = struct «FOR prtid : 0 ..< this.max_port_count SEPARATOR ' | '»«port_name(prtid)»«ENDFOR»;
			% Id's of the data stores used
			sort StoreId      = struct non_store«FOR stid : this.module.store_list BEFORE ' | ' SEPARATOR ' | '»«stid»«ENDFOR»;
			% Used to identify the type of node
			sort NodeType     = struct type_control | type_process;
			sort FlowIdAction = struct flow_generate | flow_consume | 
			                           flow_query;
			% The phase to execute
			sort Phase        = struct exec_control | exec_process;			
			sort Rights       = struct read_access_begin | read_access_done | write_access_begin | 
			                           write_access_done | rights_done;
			sort StoreMap     = StoreId -> Int;			
		'''
	}

	def get_proc_block() { //
		return (
			"\nproc\n" + this.get_node_cdfd_block() + this.get_proc_cdfd_block() + this.get_proc_enviroment_block()
		)
	}

	def get_node_cdfd_block() {
		return '''
			NodeStoreAcess(ndid:NodeId, read_store_list:List(StoreId), write_store_list:List(StoreId))  = 
				store_update_to_process(write_store_list).store_update_to_process(read_store_list).
				(#read_store_list > 0) -> 
						node_store_update(head(read_store_list),  read_access_begin).store_update_to_process(tail(read_store_list)).NodeStoreAcess(ndid, tail(read_store_list), write_store_list)
				<> (#write_store_list > 0) -> 
						node_store_update(head(write_store_list),  write_access_begin).store_update_to_process(tail(write_store_list)).NodeStoreAcess(ndid, read_store_list, tail(write_store_list))
				<> node_done_access_update.node_store_check.(sum fset: FSet(FlowId).node_flow_query(fset).sum ipid:PortId.
						(ipid in get_list_of_fire_ports(ndid, fset)) ->
							node_select_in_port(ipid).NodeFlowIdConsume(ndid, inPortDataFlowId(ndid, ipid), ipid)
				);
			
			NodeStoreRelease(ndid:NodeId, read_store_list:List(StoreId), write_store_list:List(StoreId))  = 
				(#read_store_list > 0) -> node_store_update(head(read_store_list),  read_access_done).NodeStoreRelease(ndid, tail(read_store_list), write_store_list)
				<> (#write_store_list > 0) -> node_store_update(head(write_store_list),  write_access_done).NodeStoreRelease(ndid, read_store_list, tail(write_store_list))
				<> 	node_end(ndid).node_store_check.Node(ndid);
			
			NodeFlowIdConsume(ndid:NodeId, f_set : FSet(FlowId), pid:PortId) =
				(f_set == {}) ->
					skip.NodeGenerate(ndid, pid)
				<>
					sum f: FlowId . (f in f_set) ->
						node_flow_consume(f).NodeFlowIdConsume(ndid, f_set - {f}, pid);
			
			NodeFlowIdGenerate(ndid:NodeId, f_set : FSet(FlowId), ipid:PortId, opid:PortId) =
				(f_set == {}) ->
					fire_ports(ndid, ipid, opid).NodeStoreRelease(ndid, node_read_list(ndid), node_write_list(ndid))
				<>
					sum f: FlowId . (f in f_set) -> node_flow(f).NodeFlowIdGenerate(ndid, f_set - {f}, ipid, opid);
						
			NodeGenerate(ndid:NodeId, ipid:PortId) =
				sum opidg:PortId.(opidg in io_relation(ndid, ipid)) -> selected_port_generate(ndid, opidg).NodeFlowIdGenerate(ndid, outPortDataFlowId(ndid, opidg), ipid, opidg);
			
			Node(ndid:NodeId) = 
				node_execute(ndid).fire(ndid).NodeStoreAcess(ndid, node_read_list(ndid), node_write_list(ndid));
			
		'''
	}

	def get_proc_cdfd_block() {
		return '''
			CdfdIO = 
				sum ipid: PortId.(ipid in cdfd_inputPort) -> 
						cdfd_input(ipid).CdfdGenerate(ipid, cdfdInputFlowIds(ipid));
			
			
			CdfdGenerate(ipid: PortId, flow_set:FSet(FlowId)) = 
				(flow_set == {}) ->
					cdfd_cdfd_start.CdfdConsumeStart(ipid)
				<>
					(sum f: FlowId. (f in flow_set) -> node_flow(f).CdfdGenerate(ipid, flow_set - {f}));
			
			
			CdfdConsumeStart(ipid: PortId) = 
				sum f_set: FSet(FlowId). node_flow_query(f_set).
					(f_set == {}) ->skip.CdfdConsumeStart(ipid) <>
					sum opid:PortId. (opid in cdfd_outputPort) -> (
						(f_set * cdfdOutputFlowIds(opid) == cdfdOutputFlowIds(opid) && cdfdOutputFlowIds(opid) != {}) -> cdfd_consume.CdfdConsume(ipid, opid, cdfdOutputFlowIds(opid))
						<>skip.CdfdConsumeStart(ipid)
					)<>skip.CdfdConsumeStart(ipid);
			
			
			CdfdConsume(ipid: PortId, opid: PortId, flow_set:FSet(FlowId)) = 
				(flow_set == {}) ->
					cdfd_output(opid).CdfdConsumeStart(ipid)
				<>
					sum f: FlowId. (f in flow_set) -> node_flow_consume(f).CdfdConsume(ipid, opid, flow_set - {f});
					
		'''

	}

	def get_proc_enviroment_block() {
		return '''
			EnvironmentFirst = env_cdfd_start.env_new_start.Environment(exec_control, 0);
			
			Environment(phase:Phase, counter:Nat) = 
			(counter<2) -> 
				env_new_phase(counter,phase).sum f_set: FSet(FlowId).node_flow_query(f_set).(
					(#get_fire_subset(getNodeOfType(phase),f_set,phase) > 0) -> 
						exec_phase_process(getNodeOfType(phase)).EnvironmentStart(get_fire_subset(get_fire_subset(getNodeOfType(phase),f_set,phase),f_set,phase), [], phase, 0) 
					<> 
						phase_process.Environment(flip_phase(phase), counter + 1)
				)
			<>
				skip;%env_cdfd_consume.Environment(flip_phase(phase), 0);
			
			EnvironmentStartCheck(to_wait_set:List(NodeId), phase:Phase, w_count:Int) =
				(w_count == 0) -> env_move_to_end.EnvironmentEnd(to_wait_set, phase)
				<> wait_env_store_check(w_count).env_store_check.EnvironmentStartCheck(to_wait_set, phase, w_count - 1);
			
			EnvironmentStart(nd_set:List(NodeId), to_wait_set:List(NodeId), phase:Phase, w_count:Int) =
				(#nd_set == 0) ->
					env_start_done.EnvironmentStartCheck(to_wait_set, phase, w_count)
				<> 
					env_wait_to_start(head(nd_set)).env_node_start(head(nd_set)).
						EnvironmentStart(tail(nd_set),
											  to_wait_set ++ [head(nd_set)], phase, w_count + 1);
			
			EnvironmentEnd(to_wait_set:List(NodeId), phase:Phase) =
				(#to_wait_set > 0) -> (
						env_node_end(head(to_wait_set)).env_store_check.EnvironmentEnd(tail(to_wait_set), phase))
				<> (
					(phase == exec_process) ->
						env_new_start.Environment(exec_control,0)
					<> (phase == exec_control) ->
						env_new_start.Environment(exec_process,0)
				);
			
			EnvironmentFlowIds(f_set:FSet(FlowId)) =	 
					sum f_act: FlowIdAction.(
						   (f_act == flow_consume)  -> (sum c_FlowId:FlowId.(c_FlowId in f_set)  -> env_flow_consume(c_FlowId).EnvironmentFlowIds(f_set - {c_FlowId}))
						<> (f_act == flow_query)    ->                                        env_flow_query(f_set).   EnvironmentFlowIds(f_set)
						<> (f_act == flow_generate) -> (sum c_FlowId:FlowId.!(c_FlowId in f_set) -> env_flow(c_FlowId).        EnvironmentFlowIds({c_FlowId} + f_set))
					);
			
			EnvironmentStoreConstrain(read_map:StoreMap, write_map:StoreMap, sid:StoreId) =
				(write_map(sid) > 1) ->
					error_r_w(read_map(sid), write_map(sid))
				<> (write_map(sid) > 0 && read_map(sid) > 0) ->
					error_r_w(read_map(sid), write_map(sid))
				<> (write_map(sid) < 0) ->
					error_r_w(read_map(sid), write_map(sid))
				<> (read_map(sid) < 0) ->
					error_r_w(read_map(sid), write_map(sid))
				<>
					skip.EnvironmentStore(read_map, write_map);
			
			EnvironmentStore(read_map:StoreMap, write_map:StoreMap) =
				sum rid:Rights.
							(rid == write_access_begin) ->
									sum ssid:StoreId.env_store_update(ssid,  rid).EnvironmentStoreConstrain(read_map, update_access_count(ssid, write_map, 1), ssid)
							<> (rid == write_access_done) ->
									sum ssid:StoreId.env_store_update(ssid,  rid).EnvironmentStoreConstrain(read_map, update_access_count(ssid, write_map, -1), ssid)
							<> (rid == read_access_begin) ->
									sum ssid:StoreId.env_store_update(ssid,  rid).EnvironmentStoreConstrain(update_access_count(ssid, read_map, 1), write_map, ssid)
							<> (rid == read_access_done) ->
									sum ssid:StoreId.env_store_update(ssid,  rid).EnvironmentStoreConstrain(update_access_count(ssid, read_map, -1), write_map, ssid)
							<> store_store_check.EnvironmentStore(read_map, write_map);
							
		'''
	}

	def get_act_block() {
		'''		
			
			act
			error_no_ouput;    % indicate that noe output was generated, an error state model/specification translation incorrect
			store_check;       % syncronsed message that indicate store access is requested/released
			node_store_check;  % node at a state that indicate store access is requested/released
			store_store_check; % the environment to check access wrights to a data store
			env_store_check;   % the environment is at a state where access rights to a store can be checked
			cdfd_cdfd_start;   % cdfd process generated it's input data diagram ready to execute
			cdfd_start;        % synchronised message to indicate that the cdfd did receive data
			%cdfd_done:PortId;
			cdfd_input: PortId; % indicate which input port of the cdfd generated data
			cdfd_output: PortId;% indicate which output port of the cdfd consumed data
			%consume_port: FSet(FlowId);
			cdfd_consume;
			%cdfd_cdfd_consume; % indicate that the cdfd process start consuming data
			%generate_port: FSet(FlowId);
			%done_generate_port: PortId;
			%done_comsume_port: PortId;
			%port_read_store: NodeId # StoreId;
			%port_write_store: NodeId # StoreId;
			node_flow: FlowId; % process node, ready to generate data
			node_flow_consume: FlowId; % process node, read to consume data
			node_flow_query: FSet(FlowId); % process node, to query data of
			env_cdfd_start; % process environment to start with cdfd cerieving input data
			env_flow_query: FSet(FlowId); % process env query the status of flows containnig data
			env_flow_consume: FlowId; % process flow ready to for data to be consumed
			env_store_update: StoreId # Rights; % process env store to update the access status
			env_flow: FlowId;
			env_node_start: NodeId; % environment indicate a node to start execution
			env_node_end: NodeId; % environment process wait for node to complete execution
			env_new_phase: Int # Phase;
			%env_cdfd_consume; % environment allow cdfd to be consumed ....
			%env_next;
			%env_next_env_end;
			store_update: StoreId # Rights;
			node_store_update: StoreId # Rights;
			node_execute: NodeId; % indicate a node is ready to execute
			node_end: NodeId;
			done_fire:NodeId;
			flow_consume: FlowId; % a synchronised action to indicate that data is consumed
			flow_action: FlowId; % a synchronised action to indicate that data is generated
			flow_query: FSet(FlowId); % a synchronised action to indicate that the flow containing data is being queried
			fire: NodeId;		
			fire_ports: NodeId # PortId # PortId; % indicate that a node, input and output port fire
			error;
			skip;
			start_fire: NodeId;  % a synchronised message that indicate when a node fire
			selected_port_generate: NodeId # PortId; % select an ouput port of a node that will generate data
			env_move_to_end; % debug message to indicate the environment move to its EnvironmentEnd state
			env_wait_to_start:NodeId; % environment wait for a specific node to start
			env_new_start; % environment is ready for a new set of node to fire
			%phase_control;
			phase_process; % env move to state where process are allowed to execute
			wait_env_store_check:Int;
			exec_phase_process: List(NodeId); % process env, start the execution of process nodes
			env_start_done; % process env done starting nodes to fire
			store_update_to_process: List(StoreId); % process node, to update access to data stores
			node_select_in_port:PortId; % node select inport that will consume data
			node_done_access_update; % indicate node is done updating access to data store
			error_r_w:Int # Int; % indicate an data store access violation
			
		'''
	}

	def get_eqn_block() {
		'''
			
			eqn
			
			
			(exec_process == v_phase) -> 
				flip_phase(v_phase) = exec_control;
			(exec_control == v_phase) -> 
				flip_phase(v_phase) = exec_process;
				
			get_list_of_fire_ports(nd_id, f_set1) = filter_fire_ports(nd_id, f_set1, nodeInPorts(nd_id), []);
			
			(#prt_lst_in == 0) -> 
				filter_fire_ports(nd_id, f_set1, prt_lst_in, prt_list_out) = prt_list_out;
			
			(#prt_lst_in > 0 && can_port_consume(nd_id, head(prt_lst_in), f_set1)) -> 
				filter_fire_ports(nd_id, f_set1, prt_lst_in, prt_list_out)  = 
					filter_fire_ports(nd_id, f_set1, tail(prt_lst_in),[head(prt_lst_in)] ++ prt_list_out);
			
			(#prt_lst_in > 0 && !can_port_consume(nd_id, head(prt_lst_in), f_set1)) -> 
				filter_fire_ports(nd_id, f_set1, prt_lst_in, prt_list_out) = 
					filter_fire_ports(nd_id, f_set1, tail(prt_lst_in), prt_list_out);
			
			(true) -> init_store(n)=0;
			% (true) -> allNodes = [«FOR node_name : this.module.node_list.keySet SEPARATOR ','»«node_name(node_name)»«ENDFOR»];
			% (true) -> allControlNodes =[«FOR node_name : this.module.node_list.filter[key, value| value.type != NodeInterface.Type.process] .keySet SEPARATOR ','»«node_name(node_name)»«ENDFOR»];
			% (true) -> allProcessNodes =[«FOR node_name : this.module.node_list.filter[key, value| value.type == NodeInterface.Type.process] .keySet SEPARATOR ','»«node_name(node_name)»«ENDFOR»];
			«FOR node_name : this.module.node_list.keySet»
				«FOR port: this.module.node_list.get(node_name).in_ports»
					(«node_name(node_name)» == nd_id && «port_name(port.port_index)» == pid) -> inPortDataFlowId(nd_id, pid) = {«FOR FlowId: port.get_flow_list() SEPARATOR ','»«FlowId.flow_name»«ENDFOR»};
				«ENDFOR»
			«ENDFOR»
			
			«FOR node_name : this.module.node_list.keySet»
				«FOR port: this.module.node_list.get(node_name).out_ports»
					(«node_name(node_name)» == nd_id && «port_name(port.port_index)» == pid) -> outPortDataFlowId(nd_id, pid) = {«FOR FlowId: port.get_flow_list() SEPARATOR ','»«FlowId.flow_name»«ENDFOR»};
				«ENDFOR»
			«ENDFOR»
			
			«FOR node_name : this.module.node_list.keySet»
				(«node_name(node_name)» == nd_id) -> nodeInFlowIds(nd_id) = {«FOR flw:this.module.node_list.get(node_name).get_all_in_flows SEPARATOR ','» «flw.flow_name» «ENDFOR»};
			«ENDFOR»
			
			
			«FOR node_name : this.module.node_list.keySet»
				(«node_name(node_name)» == nd_id) -> node_write_list(nd_id) = [«FOR store:this.module.node_list.get(node_name).store_write SEPARATOR ','» «store» «ENDFOR»];
			«ENDFOR»
			«FOR node_name : this.module.node_list.keySet»
				(«node_name(node_name)» == nd_id) -> node_read_list(nd_id) = [«FOR store:this.module.node_list.get(node_name).store_read SEPARATOR ','» «store» «ENDFOR»];
			«ENDFOR»
						
						
			«FOR node_name : this.module.node_list.keySet»
				«IF this.module.node_list.get(node_name).in_ports.length == 0»
					(«node_name(node_name)» == nd_id) -> nodeInPorts(nd_id) = [];
				«ELSE»
					«FOR port_index: 0..<this.module.node_list.get(node_name).in_ports.length
						BEFORE '''(«node_name(node_name)» == nd_id) -> nodeInPorts(nd_id) = [''' 
						SEPARATOR ',' 
						AFTER '];'»«port_name(port_index)»«ENDFOR»
				«ENDIF»
			«ENDFOR»
			
			«FOR node_name : this.module.node_list.keySet»
				«IF this.module.node_list.get(node_name).out_ports.length == 0»
					(«node_name(node_name)» == nd_id) -> nodeOutPorts(nd_id) = [];
				«ELSE»
					«FOR port_index: 0..<this.module.node_list.get(node_name).out_ports.length
						BEFORE '''(«node_name(node_name)» == nd_id) -> nodeOutPorts(nd_id) = [''' 
						SEPARATOR ',' 
						AFTER '];'»«port_name(port_index)»«ENDFOR»
				«ENDIF»
			«ENDFOR»
			«FOR node_name : this.module.node_list.keySet»
				«FOR iport: 0..<this.module.node_list.get(node_name).in_ports.length»
					(«node_name(node_name)» == nd_id && «port_name(iport)» == pid) -> io_relation(nd_id, pid) = {«FOR oport: 0..<this.module.node_list.get(node_name).io_relation.get(iport).size SEPARATOR ','»«port_name(oport)»«ENDFOR»};
				«ENDFOR»
			«ENDFOR»
			(«node_name(this.module.get_node_name)» == nd_id) -> nodeInPorts(nd_id) = [«FOR port_index : 0..<this.module.in_port_list.length SEPARATOR ','»«port_name(port_index)»«ENDFOR»];
			(«node_name(this.module.get_node_name)» == nd_id) -> nodeOutPorts(nd_id) = [«FOR port_index : 0..<this.module.out_port_list.length SEPARATOR ','»«port_name(port_index)»«ENDFOR»];
			(true) -> cdfd_inputPort = {«FOR port : 0..< this.module.in_port_list.length SEPARATOR ','»«port_name(port)»«ENDFOR»};
			(true) -> cdfd_outputPort = {«FOR port : 0..< this.module.out_port_list.length SEPARATOR ','»«port_name(port)»«ENDFOR»};
			«FOR port : this.module.in_port_list»
				«IF port.get_flow_list().length == 0»
					(«port_name(port.port_index)» == pid) -> cdfdInputFlowIds(pid) = {};
				«ELSE»
					«FOR flow: port.get_flow_list() 
						BEFORE '''(«port_name(port.port_index)» == pid) -> cdfdInputFlowIds(pid) = {''' 
						SEPARATOR ',' 
						AFTER '};'»«flow.flow_name»«ENDFOR»
				«ENDIF»
			«ENDFOR»		
				
			«FOR port : this.module.out_port_list»
				«IF port.get_flow_list().length == 0»
					(«port_name(port)» == pid) -> cdfdOutputFlowIds(pid) = {};
				«ELSE»
					«FOR FlowId: port.get_flow_list() 				
						BEFORE '''(«port_name(port)» == pid) -> cdfdOutputFlowIds(pid) = {''' 
						SEPARATOR ',' 
						AFTER '};'»«FlowId.flow_name»«ENDFOR»
				«ENDIF»
			«ENDFOR»
			(node_type == exec_control) -> getNodeOfType(node_type) = [«FOR node : this.module.node_list.values.filter[it| it.type == NodeInterface.Type.control] SEPARATOR ','»«node_name(node)»«ENDFOR»];
			(node_type == exec_process) -> getNodeOfType(node_type) = [«FOR node : this.module.node_list.values.filter[it| it.type == NodeInterface.Type.process && it.name != "Init"] SEPARATOR ','»«node_name(node)»«ENDFOR»];
			(true) -> processStoresRead(nd_id) = {}; 
			(true) -> processStoresWrite(nd_id) = {};
			
			(nd_id == «node_name(this.module.get_node_name)») -> can_port_consume(nd_id, pid, f_set1) = false;
			(nd_id == «node_name(this.module.get_node_name)») -> can_port_generate(nd_id, pid, f_set1) = false;
			
			(true) -> can_port_consume(nd_id, pid, f_set1) = inPortDataFlowId(nd_id, pid) * f_set1 == inPortDataFlowId(nd_id, pid);
			(true) -> can_port_generate(nd_id, pid, f_set1) = (outPortDataFlowId(nd_id, pid) - nodeInFlowIds(nd_id))* f_set1 == {};
			
			(nd_id == «node_name(this.module.get_node_name)») -> can_fire(nd_id, f_set1) = false;
			(true) -> can_fire(nd_id, f_set1) = can_consume(nd_id, f_set1) && (can_generate(nd_id, f_set1));
									
			(true) -> can_generate(nd_id, f_set1) = forall pid: PortId . pid in nodeOutPorts(nd_id) => can_port_generate(nd_id, pid, f_set1);
			(true) -> can_consume(nd_id, f_set1) = exists pid: PortId . (pid in nodeInPorts(nd_id)) && can_port_consume(nd_id, pid, f_set1);
			
			(true) -> update_access_count(sid, store_map, int_var) = store_map[sid -> store_map(sid) + int_var];
			
			(#n_list1 > 0 && can_fire(head(n_list1), f_set2)) ->  get_fire_subset(n_list1, f_set2, v_phase) = get_fire_subset(tail(n_list1), f_set2, v_phase) ++ [head(n_list1)];
			(#n_list1 > 0 && !can_fire(head(n_list1), f_set2)) ->  get_fire_subset(n_list1, f_set2, v_phase) = get_fire_subset(tail(n_list1), f_set2, v_phase);
			(#n_list1 == 0 ) ->  get_fire_subset(n_list1, f_set2, v_phase) = []; 
		'''
	}

	def get_var_block() {
		'''
			
			var
			v_phase:Phase;
			n:StoreId;
			int_var: Int;
			store_map: StoreMap;
			flw: FlowId;
			node_type: Phase;
			n_list1: List(NodeId);
			f_list1: List(FlowId);
			f_list2: List(FlowId);
			f_list3: List(FlowId);
			f_set1: FSet(FlowId);
			f_set2: FSet(FlowId);
			f_set3: FSet(FlowId);
			sid: StoreId;
			store_set: FSet(StoreId);
			store_list: FSet(StoreId);
			pid: PortId;
			nd_id: NodeId;
			prt_list_out: List(PortId);
			prt_lst_in: List(PortId);
		'''
	}

	def get_maps_block() {
		'''
			
			map
			flip_phase: Phase ->Phase;
			init_store:StoreId->Int;
			%%
			% Diagram information functions
			%%
			% The input flow of the digram and a specific input port
			cdfdInputFlowIds: PortId -> FSet(FlowId);
			% The output flow of the digram and a specific input port
			cdfdOutputFlowIds: PortId -> FSet(FlowId);
			% the list of nodes that participate in the flow diagram
			%allNodes: List(NodeId);
			% list of all the control nodes in the diagram
			%allControlNodes: List(NodeId);
			% list of all the process nodes in the diagram
			%allProcessNodes: List(NodeId);
			% the input ports of the diagram
			cdfd_inputPort: FSet(PortId);
			% the output ports of the diagram
			cdfd_outputPort: FSet(PortId);			
			% get list of ports that are allowed to fire for a specific node
			get_list_of_fire_ports:NodeId # Set(FlowId) -> List(PortId);
			%%
			% Node structure functions
			%%
			% the inflow of a node
			nodeInFlowIds: NodeId -> FSet(FlowId);
			% the stores the process access with read access
			processStoresRead: NodeId -> FSet(StoreId);
			% the stores the process access with write access
			processStoresWrite: NodeId -> FSet(StoreId);
			io_relation: NodeId # PortId -> FSet(PortId);
			% get the nodes that are allowrd to execute in the given phase
			getNodeOfType: Phase -> List(NodeId);
			% the connected in flows to a node and port
			inPortDataFlowId: NodeId # PortId -> FSet(FlowId);
			% the connected out flows to a node and port
			outPortDataFlowId: NodeId # PortId -> FSet(FlowId);
			% the input ports of a node
			nodeInPorts: NodeId -> List(PortId);
			% the output ports of a node
			nodeOutPorts: NodeId -> List(PortId);
			%%
			% Node fire functions
			%%
			% determine is node is allowed to fire
			can_fire: NodeId # FSet(FlowId) -> Bool;
			% get the sublift of nodes that are allowed to fire
			get_fire_subset: List(NodeId) # FSet(FlowId) # Phase -> List(NodeId);
			% determine if a port can consume data
			can_consume: NodeId # FSet(FlowId)-> Bool;
			% determine is a port of a node can fire
			can_port_consume: NodeId # PortId # FSet(FlowId)-> Bool;
			% determine is a port of a node can generate data
			can_port_generate: NodeId # PortId # FSet(FlowId)-> Bool;
			% determine if node can generate data
			can_generate: NodeId # FSet(FlowId) -> Bool;
			% filter ports of node that are allowed to fire
			filter_fire_ports:NodeId # Set(FlowId) # List(PortId) # List(PortId) ->  List(PortId);
			%%
			% Store functions
			%%
			store_rights: StoreId -> Rights;
			% update the store access by additing the interger parameter
			update_access_count: StoreId # StoreMap # Int -> StoreMap;
			node_read_list: NodeId -> List(StoreId);
			node_write_list: NodeId -> List(StoreId);
		'''
	}

	protected def get_var_name(String name) {
		return 'var_' + name
	}

	protected def add_store_sort(Module module) {
		this.stores_ids.addAll(module.data_stores)
	}

	protected def get_enviroment_instance(Module module) {
		var cdfd_input_list = new ArrayList<String>
		var output_list = new ArrayList<String>
		var instance = '''
			enviroment(
				[«FOR port : cdfd_input_list SEPARATOR ','»
						«port»
					«ENDFOR»],
				[«FOR port : output_list SEPARATOR ','»
						«port»
					«ENDFOR»],
				{},
				[«FOR nd : this.add_get_nodes(module) SEPARATOR ','»
						«nd»
					«ENDFOR»],
				{«FOR st : this.stores_ids SEPARATOR ','»
						«st»
					«ENDFOR»env_store_update},
				{}
			)
		'''
		return instance
	}

	protected def add_get_nodes(Module module) {
		val node_instance_list = new ArrayList<String>
		val FlowIds = module.get_FlowIds
		module.get_nodes.forEach [ k_node_name, node |
			val node_name = node_name(k_node_name)
			var in_FlowIds = new ArrayList<Flow>
			var out_FlowIds = new ArrayList<Flow>
			in_FlowIds.addAll(FlowIds.filter[Flow f|f.dest_name == node.name])
			out_FlowIds.addAll(FlowIds.filter[Flow f|f.src_name == node.name])
			node_instance_list.add(get_node_instance(node, node_name, in_FlowIds, out_FlowIds))
		]
		return node_instance_list
	}

	protected def get_node_instance(Node node, String node_name, List<Flow> in_FlowIds, List<Flow> out_FlowIds) {
		var in_port_index = 0
		var out_port_index = 0
		var instance = '''			
		process(«node_name»,
			{«FOR ip : node.in_ports»
					«this.get_port_instance(ip, in_port_index++, in_FlowIds)»,
				«ENDFOR»},
			{«FOR op : node.out_ports»
					«this.get_port_instance(op, out_port_index++, out_FlowIds)»,
				«ENDFOR»},
			{«FOR st : node.store_read»
					«this.store_name(st)»,
				«ENDFOR»},
			{«FOR st : node.store_write»
					«this.store_name(st)»,
				«ENDFOR»}
		)'''

		return instance
	}

	protected def store_name(String name) {
		return name
	}

	protected def get_port_instance(Port port, int port_index, List<Flow> FlowIds) {
		var instance = 
			'''
			port(
			«port_name(port_index)»
			,{«FOR vn : port.variable_list»
				«FlowIds.findFirst[Flow f| f.src_var == vn]»,
			«ENDFOR»}
			)'''
		return instance
	}

	static def node_name(Node node) {
		return 'NI' + node.name
	}

	static def node_name(String index) {
		return 'NI' + index

	}
	

	static def port_name(Port port) {
		return "IP" + port.port_index
	}
	static def port_name(int index) {
		return "IP" + index
	}

}

package com.gitlab.johanvdberg.sofl_editor.generator

import java.util.ArrayList
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.xbase.lib.Pair
//import org.eclipse.xtext.util.Triple
import java.util.List
import com.gitlab.johanvdberg.sofl_editor.consistency.MuCalculus
import com.gitlab.johanvdberg.sofl_editor.semantics.Module

class ConsistencyGenerate {

	protected IFileSystemAccess2 fsa
	protected MuCalculus mu
	protected Module module
	protected String base_location
	protected String script_location

	public String store_access_lps
	public String store_access_lts
	public String node_fire_lps
	public String node_fire_lts
	public String node_port_fire_lps
	public String node_port_fire_lts
	public String no_debug_lts
	public String no_debug_lps
	public String cdfd_io_lts
	public String cdfd_io_lps
	public String full_lps
	public String full_reduced_lps
	public String full_reduced_lts
	public String mcrl2_file_name

	public List<Pair<String, String>> test_list_cdsd_exec
	public List<Pair<Pair<String, String>, String>> test_list_cdsd_io_multiple_out
	public List<Pair<Pair<String, String>, String>> test_list_cdsd_io
	public List<Pair<Pair<String, String>, String>> test_list_cdsd_multiout
	public List<Pair<String, String>> test_list_process_infinite
	public List<Pair<String, String>> test_list_proc_exec
	public List<Pair<String, String>> test_list_proc_exec_mports
	public List<Pair<String, String>> test_each_path_genrate_on_all
	 
	new(Module module, String base_location, IFileSystemAccess2 fsa,String mcrl2_file_name) {
		this.module = module
		this.base_location = base_location
		this.script_location = this.base_location + '/acp_scripts/'
		this.fsa = fsa
		this.mu = new MuCalculus(module)
		this.store_access_lts = 'store_access.lts'
		this.store_access_lps = 'store_access.lps'
		this.node_fire_lts = 'node_fire.lts'
		this.node_fire_lps = 'node_fire.lps'
		this.node_port_fire_lts = 'node_port_fire.lts'
		this.node_port_fire_lps = 'node_port_fire.lps'
		this.full_reduced_lts = 'full_reduced.lts'
		this.full_lps = 'full.lps'
		this.full_reduced_lps = 'full_reduced.lps'
		this.no_debug_lts = 'no_debug.lts'
		this.no_debug_lps = 'no_debug.lps'
		this.cdfd_io_lts = 'cdfd_io.lts'
		this.cdfd_io_lps = 'cdfd_io.lps'
		this.mcrl2_file_name = mcrl2_file_name
		
		this.test_list_cdsd_io  = new ArrayList<Pair<Pair<String, String>, String>>
		this.test_list_cdsd_io_multiple_out  = new ArrayList<Pair<Pair<String, String>, String>>
		this.test_list_cdsd_exec  = new ArrayList<Pair<String, String>>
		this.test_list_process_infinite  = new ArrayList<Pair<String, String>>
		this.test_list_proc_exec  = new ArrayList<Pair<String, String>>
		this.test_list_proc_exec_mports  = new ArrayList<Pair<String, String>>
		this.test_each_path_genrate_on_all  = new ArrayList<Pair<String, String>>
	}

	def write() {
		var index = 0
		var file_name = ''
		for (formula : this.mu.allPathGenerateData) {
			file_name = '''«this.script_location»«this.module.name»path_«formula.in»_generate_on_all_ouputports.mcf'''
			this.test_each_path_genrate_on_all.add(new Pair(formula.in, "." + file_name.substring(this.base_location.length)))
			fsa.generateFile(file_name, formula.formula)
			index = index + 1
		}	
		
		index = 0
		for (formula : this.mu.eachInportProcessExecute) {
			file_name = '''«this.script_location»«this.module.name»cdsa_process_exec_I_«formula.key».mcf'''
			this.test_list_cdsd_exec.add(new Pair(formula.key, "." + file_name.substring(this.base_location.length)))
			fsa.generateFile(file_name, formula.value)
			index = index + 1
		}						
		index = 0
		for (formula : this.mu.eachEachProcessExecute) {
			file_name = '''«this.script_location»«this.module.name»can_process_«formula.key»_exec.mcf'''
			this.test_list_proc_exec.add(new Pair(formula.key, "." + file_name.substring(this.base_location.length)))
			fsa.generateFile(file_name, formula.value)
			index = index + 1
		}
		index = 0
		for (formula : this.mu.moreOncePortConsume) {
			file_name = '''«this.script_location»«this.module.name»can_process_«formula.key»_comsume_twice_or_more.mcf'''
			this.test_list_proc_exec_mports.add(new Pair(formula.key, "." + file_name.substring(this.base_location.length)))
			fsa.generateFile(file_name, formula.value)
			index = index + 1
		}						
		index = 0
		for (formula : this.mu.processFireInfinite) {
			file_name = '''«this.script_location»«this.module.name»process_exec_infinite_I_«formula.key».mcf'''
			this.test_list_process_infinite.add(new Pair(formula.key, "." + file_name.substring(this.base_location.length)))
			fsa.generateFile(file_name, formula.value)
			index = index + 1
		}

		for (cdfd_info : this.mu.cdfd_in_generate) {
			file_name = '''«this.script_location»«this.module.name»cdsa_in_T_«cdfd_info.in»_generate.mcf'''
			fsa.generateFile(file_name, cdfd_info.formula)
		}

		for (cdfd_info : this.mu.cdfd_io_relation) {
			file_name = '''«this.script_location»cdsa_io_«this.module.name»_in_«cdfd_info.in»_out_«cdfd_info.out»_generate.mcf'''
			this.test_list_cdsd_io.add(
				new Pair(new Pair(cdfd_info.in, cdfd_info.out), 
					"." + file_name.substring(this.base_location.length))
			)
			fsa.generateFile(file_name,cdfd_info.formula)
		}
		
		for (cdfd_info : this.mu.cdfd_multi_output) {
			file_name = '''«this.script_location»cdsa_multiout_«this.module.name»_out_«cdfd_info.out»_generate.mcf'''
			this.test_list_cdsd_io_multiple_out.add(
				new Pair(new Pair(cdfd_info.in, cdfd_info.out), 
					"." + file_name.substring(this.base_location.length))
			)
			fsa.generateFile(file_name,cdfd_info.formula)
		}
				
		this.test_list_cdsd_io.sortInplaceBy[itr | itr.key.key + itr.key.value]
		
		var file_array = this.mcrl2_file_name.split("\\/")
		var filename = file_array.last
		fsa.generateFile(base_location + '''transform_«this.module.name».sh''',
			this.create_tansform_script(filename))
		fsa.generateFile(base_location + '''mu_calcules_«this.module.name».sh''',
			this.create_mu_calcules_scripts())
	}

	def create_mu_calcules_scripts() {
		return 
		'''
			  #!/bin/sh
			  
			  # Function to evaluate the cdsa, ports that will execute
			  cdfd_start_test () {
				«FOR formula: this.test_list_cdsd_exec»
					echo "$(tput setaf 2)Evaluate «formula.value» $(tput sgr0)"
					lps2pbes $1 -f «formula.value» «formula.value».pbes -v
					pbes2bool «formula.value».pbes > «formula.value».bool
				«ENDFOR»
			  }
			  
			  node_multi_consume () {
			  	«FOR formula: this.test_list_proc_exec_mports»
			  		echo "$(tput setaf 2)Evaluate «formula.value» $(tput sgr0)"
			  		lps2pbes $1 -f «formula.value» «formula.value».pbes -v
			  		pbes2bool «formula.value».pbes > «formula.value».bool
			  	«ENDFOR»
			  }
			  			  
			  infinite_process_test () {
			  	«FOR formula: this.test_list_proc_exec»
			  		echo "$(tput setaf 2)Evaluate «formula.value» $(tput sgr0)"
			  		lps2pbes $1 -f «formula.value» «formula.value».pbes -v
			  		pbes2bool «formula.value».pbes > «formula.value».bool
			  	«ENDFOR»
			  	«FOR formula: this.test_list_process_infinite»
			  		echo "$(tput setaf 2)Evaluate «formula.value» $(tput sgr0)"
			  		lps2pbes $1 -f «formula.value» «formula.value».pbes -v
			  		pbes2bool «formula.value».pbes > «formula.value».bool
			  	«ENDFOR»
			  }
			  
			  cdsa_io_test () {
			  «FOR formula: this.test_list_cdsd_io»
			  	echo "$(tput setaf 2)Evaluate «formula.value» $(tput sgr0)"
			  	lps2pbes $1 -f «formula.value» «formula.value».pbes -v
			  	pbes2bool «formula.value».pbes > «formula.value».bool
			  «ENDFOR»
			  }
			
			  cdsa_io_test_multi_out () {
			«FOR formula: this.test_list_cdsd_io_multiple_out»
				echo "$(tput setaf 2)Evaluate «formula.value» $(tput sgr0)"
				lps2pbes $1 -f «formula.value» «formula.value».pbes -v
				pbes2bool «formula.value».pbes > «formula.value».bool
			«ENDFOR»
				}
				each_path_generate () {
				«FOR formula: this.test_each_path_genrate_on_all»
					echo "$(tput setaf 2)Evaluate execution path «formula.key»: «formula.value» $(tput sgr0)"
					lps2pbes $1 -f «formula.value» «formula.value».pbes -v
					pbes2bool «formula.value».pbes > «formula.value».bool
				«ENDFOR»
				}
			
			      
			  analysis() {
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Test if all input ports allow execution of the diagram $(tput sgr0)"
			  	«FOR element: this.test_list_cdsd_exec»
			  		echo Does port «element.key» result in execution: `cat «element.value».bool`
			  	«ENDFOR»
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Test a node can comsume from more than input port in a fire set $(tput sgr0)"
			  	«FOR element: test_list_proc_exec_mports»
			  		echo Can node  «element.key» consume from more than one port : `cat «element.value».bool`
			  	«ENDFOR»
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Test if input ports of CDFD, allow generation of data on multiple outputs $(tput sgr0)"
			  	«FOR element: test_list_cdsd_io_multiple_out»
			  		echo Can port «element.key.key» generate results on more than one output port : `cat «element.value».bool`
			  	«ENDFOR»
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Test if processes can execute at least once $(tput sgr0)"
			  	«FOR element: this.test_list_proc_exec»
			  		echo Can process «element.key» execute at least once: `cat «element.value».bool` 
			  	«ENDFOR»
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Test if processes can execute infinite number of time $(tput sgr0)"
			  	«FOR element: this.test_list_process_infinite»
			  		echo Can process «element.key» execute infinite times: `cat «element.value».bool` 
			  	«ENDFOR»
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Test the input and ouput relations $(tput sgr0)"
			  	«FOR element: test_list_cdsd_io»
			  		echo Can port «element.key.key» generate results on port «element.key.value»: `cat «element.value».bool`
			  	«ENDFOR»
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Test if each execution generated ouput data$(tput sgr0)"
			  	«FOR element: test_each_path_genrate_on_all»
			  		echo For every execution path starting with «element.key» can ouput be generated: `cat «element.value».bool`
			  	«ENDFOR»
			  	echo
			  	echo
			  	echo "$(tput setaf 2)Give data store violation results$(tput sgr0)" 
			  	for el in `ls store*.trc.txt`
			  	do
			  		echo 
			  		 	echo 'Data store violation'
			  		 	echo $el
			  		 	cat $el
			  	done
			  	
			  }
			  
			  cdfd_start_test "«this.node_fire_lps»"
			  node_multi_consume "«this.node_port_fire_lps»"
			  infinite_process_test "«this.node_fire_lps»"
			  cdsa_io_test "«this.cdfd_io_lps»"
			  cdsa_io_test_multi_out "«this.cdfd_io_lps»"
			  each_path_generate "«this.cdfd_io_lps»"
			  analysis
			  
			  echo "Done"
		'''
	}

	def create_tansform_script(String string) {
		var out_remove_vars_lps = string.replace('.mcrl2', '_elem_var.lps')
		var out_remove_sum_lps = string.replace('.mcrl2', '_sum_elem_var.lps')
		var out_lts_1 = string.replace('.mcrl2', '.lts')

		var lts_to_lps = #[
			//#[this.full_reduced_lts, this.full_reduced_lps,''],
			#[this.node_port_fire_lts, this.node_port_fire_lps,''],
			#[this.node_fire_lts, this.node_fire_lps,''],
			#[this.store_access_lts, this.store_access_lps,'--action=error_no_ouput,error,error_r_w --error-trace --trace=3.0000000000 '],
			//#[this.no_debug_lts, this.no_debug_lps,''],
			#[this.cdfd_io_lts, this.cdfd_io_lps,'']
		]
		var to_remove_files = new ArrayList<String>
		to_remove_files.add(out_remove_vars_lps)
		to_remove_files.add(out_remove_sum_lps)
		to_remove_files.add(out_lts_1)
		// to_remove_files.add(out_remove_const_lps)		
		// to_remove_files.add(out_lts_1)
		var debug_action = #[
			"env_new_pahse",
			"node_done_access_update",
			"store_update_to_process",
			"skip",
			"exec_phase_process",
			"phase_process",
			"wait_env_store_check",
			"env_wait_to_start",
			"env_move_to_end"
		]
		var store_only_filter = #[
			'store_check',
			'flow_action',
			'flow_consume',
			'flow_query',
			'node_select_port',
			'selected_port_generate',
			'done_fire',
			'fire_ports',
			'env_start_done',			
			'cdfd_start',
			'cdsa_consume',
			'node_done_access_update',
			'node_select_in_port',
			'env_new_phase'
		]
		
		var fire_only_filter = #[
			"store_update",
			"store_check",
			'flow_query',
			'flow_consume',
			'selected_port_generate',
			'flow_action',
			'fire_ports',
			"node_select_in_port",
			"done_fire",
			"cdfd_start",
			'start_done',
			'start_fire',
			"cdsa_consume",
			'env_new_phase',
			'env_start_done'
		]
		
		var port_fire_only_filter = #[
			"store_update",
			"store_check",
			'flow_query',
			'flow_consume',
			'selected_port_generate',
			'flow_action',
			"node_select_in_port",
			"done_fire",
			"cdfd_start",
			'start_done',
			'start_fire',
			"cdsa_consume",
			'env_new_phase',
			'env_start_done'
		]
		
		var cdfd_consume_generate = #[
			'flow_ation',
			'flow_consume',
			'flow_query',
			'fire',
			'flow_action',
			'env_start_done',
			'env_new_start',
			"env_new_pahse",
			"node_done_access_update",
			"store_update_to_process",
			"selected_port_generate",
			"skip",
			"phase_process",
			"wait_env_store_check",
			'env_new_phase',
			"env_wait_to_start",
			"env_move_to_end",			
			"done_fire",
			"cdfd_start",
			"cdsa_consume",			
			'fire_ports',
			'start_fire',
			"node_select_in_port",
			'node_select_port',
			'node_done_access_update',
			'node_select_in_port',
			'store_update_to_process',
			'store_check',
			"store_update",
			'error_r_w'
		]
		
		var equivalence = 'weak-trace'
		return '''
			#!/bin/sh
			echo "$(tput setaf 2)clean the environment $(tput sgr0)"
			rm *.lts
			rm *.lps
			
			echo "$(tput setaf 2)Convert the mcrl2 descripton to a lps file $(tput sgr0)"
			time mcrl22lps «string»  «this.full_lps» --cluster --lin-method=regular2 --rewriter=jitty --verbose 
			echo $?
			if [ "$?" -ne 0 ] 
			then
				echo "$(tput setaf 2)Conversion from mcrl2 to lps to failed$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Remove redandent elements $(tput sgr0)"
			time lpsparelm «this.full_lps» «out_remove_vars_lps» --verbose
			if [ "$?" -ne 0 ] 
			then
				echo "$(tput setaf 1)Failed lpsparelm$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Remove redandent elements $(tput sgr0)"
			time lpssumelm «out_remove_vars_lps» «out_remove_sum_lps» --verbose
			if [ "$?" -ne 0 ] 
			then
				echo "$(tput setaf 1)Failed lpssumelm$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Remove redandent elements $(tput sgr0)"
			time lpsconstelm «out_remove_sum_lps» «this.full_lps» --verbose
			if [ "$?" -ne 0 ] 
			then
				echo "$(tput setaf 1)Failed to remove unused varaibles$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Convert the simplified lps to lts $(tput sgr0)"
			time lps2lts «this.full_lps» «out_lts_1» --cached  --rewriter=jitty --strategy=depth --verbose -D   
			if [ "$?" -ne 0 ] 
			then
				echo "$(tput setaf 1)lps to lts conversion failed$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Create a lts model reduce by using bisimulation $(tput sgr0)"
			time ltsconvert «out_lts_1» «this.full_reduced_lts» --equivalence=weak-bisim --verbose
			if [ "$?" -ne 0 ] 
			then
				echo "$(tput setaf 1)Convert bi-simulation failed$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Remove all debug variables «this.full_reduced_lts» «this.no_debug_lts» $(tput sgr0)"
			ltsconvert  «this.full_reduced_lts» «this.no_debug_lts» --equivalence=«equivalence» «FOR act: debug_action BEFORE '--tau=' SEPARATOR ','»«act»«ENDFOR» --verbose
			if [ "$?" -ne 0 ]
			then
				echo "$(tput setaf 1)Hiding variables failed when creating production$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Convers «this.no_debug_lts» «this.store_access_lts» $(tput sgr0) "
			ltsconvert  «this.no_debug_lts» «this.store_access_lts» --equivalence=«equivalence» «FOR act: store_only_filter BEFORE '--tau=' SEPARATOR ','»«act»«ENDFOR»  --verbose
			if [ "$?" -ne 0 ]
			then
				echo "$(tput setaf 1)Hiding variables failed when creating production stopre access$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Convers «this.no_debug_lts» «this.node_fire_lts» $(tput sgr0) "
			ltsconvert  «this.no_debug_lts» «this.node_fire_lts» --equivalence=«equivalence» «FOR act: fire_only_filter BEFORE '--tau=' SEPARATOR ','»«act»«ENDFOR» --verbose
			if [ "$?" -ne 0 ]
			then
				echo "$(tput setaf 1)Hiding variables failed$(tput sgr0)"
				exit
			fi
			
			echo "$(tput setaf 2)Convers «this.no_debug_lts» «this.node_port_fire_lts»$(tput sgr0) "
			ltsconvert  «this.no_debug_lts» «this.node_port_fire_lts» --equivalence=«equivalence» «FOR act: port_fire_only_filter BEFORE '--tau=' SEPARATOR ','»«act»«ENDFOR» --verbose
			if [ "$?" -ne 0 ]
			then
				echo "$(tput setaf 2)Hiding variables failed$(tput sgr0)"
				exit
			fi
			
			
			echo "$(tput setaf 2)Only keep the cdfd condume and generate action «this.no_debug_lts» «this.cdfd_io_lts»$(tput sgr0)"
			ltsconvert  «this.no_debug_lts» «this.cdfd_io_lts» --equivalence=«equivalence» «FOR act: cdfd_consume_generate BEFORE '--tau=' SEPARATOR ','»«act»«ENDFOR» --verbose
			if [ "$?" -ne 0 ]
			then
				echo "$(tput setaf 1)Hiding variables failed when creating production$(tput sgr0)"
				exit
			fi
			
			
			cp «this.full_lps» «this.full_reduced_lps»
			«FOR pair : lts_to_lps»
				echo "$(tput setaf 2)Create each model with simplifications «pair.get(0)» «pair.get(1)» $(tput sgr0)"
				time lts2lps «pair.get(0)» «pair.get(1)» --verbose
				echo "lps2lts «pair.get(1)» «pair.get(0)» «pair.get(2)» --rewriter=jitty --strategy=breadth  --verbose"
				lps2lts «pair.get(1)» «pair.get(0)» «pair.get(2)» --rewriter=jitty --strategy=breadth  --verbose
				if [ "$?" -ne 0 ]
				then
					echo "$(tput setaf 2)convert to lps failed$(tput sgr0)"
					exit
				fi
			«ENDFOR»
			«FOR file : to_remove_files»
				rm «file»
			«ENDFOR»
			
			for file in `ls *.trc`; 
			do
				echo $file
				tracepp $file ${file}.txt --format=plain --verbose
			done
			
			echo "Done"
		'''
	}

}



package com.gitlab.johanvdberg.sofl_editor.generator.smt

import com.gitlab.johanvdberg.sofl_editor.smt.SmtConditionNode
import org.eclipse.xtext.generator.IFileSystemAccess2
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFile
import java.util.List
import java.util.HashMap
import java.util.Map

class SmtProveControl {
	public SmtConditionNode node
	public SmtFile smt_file
	public Map<String, SmtFile> smt_transition_file

	new(SmtConditionNode node, String file_prefix) {
		this.node = node
		this.smt_transition_file = new HashMap<String, SmtFile>
		this.smt_file = new SmtFile(
			file_prefix,
			'''condition_all_ouput_condition_«this.node.name».smt''',
			SmtFile.ProofType::Constrain,
			this.node.flow_port_obligations.values.head.smt_proof
		)
		for (key : this.node.transition_obligations.keySet) {
			this.smt_transition_file.put(
				key.toString,
				new SmtFile(file_prefix, '''condition_int_out«this.node.name»_«key».smt''',
					SmtFile.ProofType::Constrain, this.node.transition_obligations.get(key).smt_proof)
			)
		}
	}

	def write_markdown() {
		'''
			Condition: «node.get_name()»
			«FOR i : 0..<node.get_name.length + 11»-«ENDFOR»
			
			The conditions of the different ports are
			
			| Port condition | |
			|----------------|-|
			«FOR port : this.node.get_inner_output_ports»
				|«port.get_variables.head»|«port.get_codition.toString.replace("\n", " ")»|
			«ENDFOR»
			
			The proof script is given by:
			«this.smt_file.write_markdown»
		'''
	}

	def write_file(IFileSystemAccess2 fase) {
		this.smt_file.write_file(fase)
		for(current: this.smt_transition_file.values){
			current.write_file(fase)
		}
	}

}

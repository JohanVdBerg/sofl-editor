package com.gitlab.johanvdberg.sofl_editor.generator.smt

import java.util.HashMap
import java.util.Map
import org.eclipse.xtext.generator.IFileSystemAccess2
import com.gitlab.johanvdberg.sofl_editor.smt.logic.SmtCondition
import com.gitlab.johanvdberg.sofl_editor.smt.SmtNode
import com.gitlab.johanvdberg.sofl_editor.smt.SmtTransitionPorts
import com.gitlab.johanvdberg.sofl_editor.smt.file.SmtFile

class SmtProveNode {
	
	String name
	public Map<String, SmtFile> proof_start
	public Map<SmtTransitionPorts, SmtFile> proof_end
	SmtNode node
	//String file_base
	
	new(SmtNode node, String file_base) {
		this.node = node
		//this.file_base = file_base
		this.name = node.name
		this.proof_start = new HashMap<String, SmtFile>
		this.proof_end = new HashMap<SmtTransitionPorts, SmtFile>
		
		for(port_id: node.flow_port_obligations.keySet){
			this.proof_start.put(port_id, 
				new SmtFile(file_base,'''node_«this.get_name»_inport_«port_id.trim».smt2''', 
					SmtFile::ProofType::StartState, 
					node.flow_port_obligations.get(port_id).smt_proof
				)
			)
		}
		
		for(port_id: node.transition_obligations.keySet){
			this.proof_end.put(port_id, 
				new SmtFile(file_base, '''node_«this.get_name»_outport_«port_id.toString.replace(' ','_')».smt2''',
					SmtFile::ProofType::EndState, 
					node.transition_obligations.get(port_id).smt_proof
				)
			)
		}
	}
	
	def get_name(){
		this.name
	}	
	
	def write_file(IFileSystemAccess2 fs){
		for(now : this.proof_end.values){
			now.write_file(fs)
		}
		for(now : this.proof_start.values){
			now.write_file(fs)
		}
	}
	
	def write_markdown(){
		'''
		Node: «node.get_name()»
		«FOR i:0..<node.get_name.length + 6»-«ENDFOR»
		
		The pre/port condition table of the node is given by 
		
		«FOR i:0..< node.get_inner_output_ports.length BEFORE '| |' » «i+1» |	«ENDFOR»
		«FOR i:0..< node.get_inner_output_ports.length BEFORE '|-|' »---- |	«ENDFOR»
		«FOR r:0..< node.get_inner_input_ports.length  »|«r+1» | «FOR c:0..< node.get_inner_output_ports.length» `«node.getDestinationPredicate(r, c).get_formula.smt.replace("\n"," ")»` ~~~> `«node.getSourcePredicate(r,c).get_formula.smt.replace("\n"," ")»` | «ENDFOR»
		«ENDFOR»
		
		The invariant
		
				«SmtCondition::conjunction(node.parent.get_invariant).get_formula.smt»
		
		is imposed by the module on the node's execution context.
		
		To verify a node the following needs to be done:
		«IF node.have_parent»
			- When a node executes the environment is defined by the invariant of the module and all the condition of output 
			  ports that provide data to the current node. Conjunctions of these condition create the environment and the condition
			  of the current port must be valid.
			- After execution of node the terminating port must be valid in the environment. The environment is defined by conjunction
			  between the condition of the port that started the execution and invariant fo the containing process.
		«ELSE»
			Since this a root node the input port that start the execution is assumed to be true. Thus only the validity of the post 
			conditions needs to be verified.
		«ENDIF»
		
		«IF node.is_decomposed»
			The node is decomposed and the refinde module needs to be verified.
		«ELSE»
			The node is not decomposed the prove obligation only need to be satifiable.
		«ENDIF»
		
		### Input ports (in flow)
		
		Proof obligations of input data flows
		«FOR in_obj: node.flow_port_obligations.keySet»
			#### Port «in_obj»
			«SmtProveModule::mark_down(node.flow_port_obligations.get(in_obj))»
			«this.proof_start.get(in_obj).write_markdown»
		«ENDFOR»
		
		### Output/Inport ports transitions
		
		Proof obligations due to transitions
		«FOR in_obj: node.transition_obligations.keySet»
			#### Port «in_obj»
			«SmtProveModule::mark_down(node.transition_obligations.get(in_obj))»
			«this.proof_end.get(in_obj).write_markdown»
		«ENDFOR»
		'''
	}
	
}
package com.gitlab.johanvdberg.sofl_editor.generator

import org.eclipse.xtext.generator.IFileSystemAccess2
import com.gitlab.johanvdberg.sofl_editor.generator.smt.SmtProveModule
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.smt.SmtModule
import com.gitlab.johanvdberg.sofl_editor.smt.SmtNode

class SmtGenerator {
	SmtModule smtModule
	String dotfile
	String dotfile_name
	String smtScripts
	String base_location
	IFileSystemAccess2 fsa
	SmtProveModule moduleProve
	
	new(SoflModule module, String base_location, IFileSystemAccess2 fsa){
		this.smtModule = new SmtModule(module.name, module.cdfd)
		this.dotfile_name = '''«module.name».dot'''
		this.dotfile = '''«base_location»/«this.dotfile_name»'''
		this.smtScripts = '''«base_location»/smt_script'''
		this.moduleProve = new SmtProveModule(this.smtModule, this.smtScripts, fsa)
		this.base_location = base_location
		this.fsa = fsa
	}
	
	
	def write(){
		this.write_dot_graph
		//this.write_prove_script
		this.moduleProve.write_markdown
	//}		
	
	//def write_prove_script(){
		//write the file content
		this.moduleProve.write_smt
		this.moduleProve.write_script
		this.moduleProve.write_markdown
		
		var semantc_files = #[
			'''transform_«this.smtModule.name».sh''',
			'''mu_calcules_«this.smtModule.name».sh'''			
		]
		var semantic_context = 
		'''
		echo "Perform semantic analysis"
		«FOR file: semantc_files»
		chmod u+x «file»
		./«file»
		«ENDFOR»
		cd smt_script
		chmod u+x prove_smt.sh
		./prove_smt.sh
		cd ..
		'''
		fsa.generateFile('''«this.base_location»/semantics.sh''',semantic_context)
		
		var all_context =
		'''
		chmod u+x semantics.sh
		./semantics.sh
		dot `ls *.dot` -Tpng -o cdfd.png
		'''
		//fsa.generateFile('''«this.base_location»/all.sh''',all_context)
	}
			
	def write_dot_graph(){
		var text = '''
			digraph «this.smtModule.name»{
				 rankdir=LR;
			«FOR node: this.smtModule.getAllNodes»
				«dot(node)»
			«ENDFOR»
			«FOR node: this.smtModule.getAllNodes»
				«dot_flow(node)»
			«ENDFOR»
			}
		'''
		fsa.generateFile(this.dotfile,text)
	}
	
	def dot(SmtNode n){
		'''
			«n.name»  [shape=record,label="
				{«FOR port:n.get_outer_destination_ports() SEPARATOR '|'»<«port.get_name»>«port.index»«ENDFOR»}
				|«n.name»|
				{«FOR port:n.get_outer_source_ports() SEPARATOR '|'»<«port.get_name»>«port.index»«ENDFOR»}"];
			
		'''		
	}
	
	def dot_flow(SmtNode n){
		var str = ''
		for(port: n.get_outer_source_ports()){
			for(a_post:port.get_connected_ports){
				str = str + '''"«port.parentName»":«port.get_name» -> "«a_post.parentName»":«a_post.get_name»;
				'''
			}				
		}
		return str
	}
}




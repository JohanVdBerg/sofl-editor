package com.gitlab.johanvdberg.sofl_editor.semantics

interface NodeInterface {
	enum Type{
		process, condition, module, control
	}
	
	def void add_node_as_destination(VariableName variable_name, NodeInterface node) { 
		var out_port = this.get_containing_ports(variable_name, false)
		var in_port = if(node instanceof Module) {
			node.get_containing_ports(variable_name, false)
		}else{
			node.get_containing_ports(variable_name, true)
		}		
		for(oport: out_port){
			for(iport: in_port){
				oport.add_associated_port(variable_name, iport)		
				iport.add_associated_port(variable_name, oport)		
			}	
		}
	}

	def String get_node_name()
	def Iterable<Port> get_containing_ports(VariableName variable_name, Boolean input)
}

 
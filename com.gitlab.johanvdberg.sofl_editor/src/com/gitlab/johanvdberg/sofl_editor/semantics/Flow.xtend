package com.gitlab.johanvdberg.sofl_editor.semantics


class Flow implements Comparable<Flow>{
	public String src_name
	public VariableName src_var
	public VariableName dest_var
	public String dest_name
	public boolean shadow
	
	override equals(Object o){
		if(o instanceof Flow){
			return (this.src_name == o.src_name) &&
				(this.src_var == o.src_var) &&
				(this.dest_name == o.dest_name) &&
				(this.dest_var == o.dest_var)
		}
		return super.equals(o)
	}
	new(String src_name, VariableName src_var, VariableName dest_var, String dest_name, boolean shadow){
		this.src_name = src_name
		this.src_var = src_var
		this.dest_var = dest_var
		this.dest_name = dest_name
		this.shadow = shadow
	}
	
	new(String src_name, VariableName src_var, VariableName dest_var, String dest_name){
		this.src_name = src_name
		this.src_var = src_var
		this.dest_var = dest_var
		this.dest_name = dest_name
		this.shadow = src_var.shadow
		if(src_var.shadow != dest_var.shadow){
			throw new RuntimeException("connected name must be of the same type")
		}
	}
	
	def flow_name(){
		return Flow.FlowId_name(this)
	}
	
	static def FlowId_name(String src_name, String src_var, String dest_var, String dest_name) {
		return src_name + src_var + '_' + dest_var + dest_name
	}
	
	static def FlowId_name(Flow FlowId) {
		return Flow::FlowId_name(FlowId.src_name, FlowId.src_var.name, FlowId.dest_var.name, FlowId.dest_name)
	}
	
	override compareTo(Flow o) {
		this.flow_name.compareTo(o.flow_name)
	}
	
	override toString() {
		return '''«this.src_name».«this.src_var»-->«this.dest_name».«this.dest_var»'''
	}
	
}
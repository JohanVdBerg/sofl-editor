package com.gitlab.johanvdberg.sofl_editor.semantics

import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConnectionName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NodeName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure
import java.util.ArrayList
import java.util.List
import java.util.Map
import java.util.TreeMap
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflVariableName

class Module implements NodeInterface {
	SoflModule module
	public Map<String, Node> node_list
	public List<String> port_names
	public List<CdfdPort> in_port_list
	public List<CdfdPort> out_port_list
	public List<String> store_list	
	public List<Flow> flow_list
	public String cdsa_name
	public String name
	public ArrayList<String> data_stores

	public int max_port_count

	new(SoflModule module) {
		System.out.println("Parse module: " + module.name)
		this.module = module
		this.name = this.module.name
		this.max_port_count = 0
		this.node_list = new TreeMap<String, Node>
		this.flow_list = new ArrayList<Flow>
		this.data_stores = new ArrayList<String>
		this.in_port_list = new ArrayList<CdfdPort>
		this.out_port_list = new ArrayList<CdfdPort>
		this.store_list = new ArrayList<String>
		// this.store_write = new ArrayList<String>
		// add processes
		for (process : module.proc_spec) {
			var node = new Node(process)
			this.node_list.put(process.name, node)
			this.max_port_count = Math.max(this.max_port_count, node.in_ports.length)
			this.max_port_count = Math.max(this.max_port_count, node.out_ports.length)
		}

		if (module.cdfd !== null) {
			// add control structure
			for (condition : module.cdfd.eContents.filter(ConditionStructure)) {
				var node = new Node(condition)
				this.node_list.put(condition.name, node)
				this.max_port_count = Math.max(this.max_port_count, node.in_ports.length)
				this.max_port_count = Math.max(this.max_port_count, node.out_ports.length)
			}

			for (condition : module.cdfd.eContents.filter(BroadcastStructure)) {
				var node = new Node(condition)
				this.node_list.put(condition.name, node)
				this.max_port_count = Math.max(this.max_port_count, node.in_ports.length)
				this.max_port_count = Math.max(this.max_port_count, node.out_ports.length)
			}

			for (condition : module.cdfd.eContents.filter(MergeStructure)) {
				var node = new Node(condition)
				this.node_list.put(condition.name, node)
				this.max_port_count = Math.max(this.max_port_count, node.in_ports.length)
				this.max_port_count = Math.max(this.max_port_count, node.out_ports.length)
			}

			for (condition : module.cdfd.eContents.filter(UnMergeStructure)) {
				var node = new Node(condition)
				this.node_list.put(condition.name, node)
				this.max_port_count = Math.max(this.max_port_count, node.in_ports.length)
				this.max_port_count = Math.max(this.max_port_count, node.out_ports.length)
			}
	
			if( module.cdfd?.grouping?.in_grouping?.ports !== null){
				this.cdsa_name = module.cdfd.name
				module.cdfd.grouping.in_grouping.ports.forEach [ port, port_index |
					val variable_list = new ArrayList<VariableName>
					if(port.vars !== null){
						variable_list.addAll(port.vars.map[new VariableName(it)])
					}
					if(port.connections !== null){
						variable_list.addAll(port.connections.map[new VariableName(it)])
					}
					port.connections.forEach[variable_list.add(new VariableName(it))]
					this.in_port_list.add(new CdfdPort(true, variable_list, this.in_port_list.length, this))
				]
			}else if (module.cdfd?.in_ports?.ports !== null) {
				// add the behavior ports
				this.cdsa_name = module.cdfd.name
				module.cdfd.in_ports.ports.forEach [ port, port_index |
					val variable_list = new ArrayList<VariableName>
					for (varialbe_name : port.eAllContents.filter(SoflVariableName).toIterable) {
						variable_list.add(new VariableName(varialbe_name))
					}
					for (varialbe_name : port.eAllContents.filter(ConnectionName).toIterable) {
						variable_list.add(new VariableName(varialbe_name))
					}
					
					port.connections.forEach[variable_list.add(new VariableName(it))]
					/*if (port.connection !== null) {
						variable_list.add(Port.empty_variable_name(port.connection.name))
					}*/
					this.in_port_list.add(new CdfdPort(true, variable_list, this.in_port_list.length, this))
				]
			}
			if( module.cdfd?.grouping?.out_grouping?.ports !== null){
				this.cdsa_name = module.cdfd.name
				module.cdfd.grouping.out_grouping.ports.forEach [ port, port_index |
					val variable_list = new ArrayList<VariableName>
					if(port.vars !== null){
						variable_list.addAll(port.vars.map[new VariableName(it)])
					}
					if(port.connections !== null){
						variable_list.addAll(port.connections.map[new VariableName(it)])
					}
					port.connections.forEach[variable_list.add(new VariableName(it))]
					this.out_port_list.add(new CdfdPort(false, variable_list, this.out_port_list.length, this))
				]
			}else if (module.cdfd?.out_ports?.ports !== null) {
				module.cdfd.out_ports.ports.forEach [ port, port_index |
					val variable_list = new ArrayList<VariableName>
					for (varialbe_name : port.eAllContents.filter(SoflVariableName).toIterable) {
						variable_list.add( new VariableName(varialbe_name))
					}
					for (varialbe_name : port.eAllContents.filter(ConnectionName).toIterable) {
						variable_list.add(new VariableName(varialbe_name))
					}
					port.connections.forEach[variable_list.add(new VariableName(it))]
					/*if (port.connection !== null) {
						variable_list.add(Port.empty_variable_name(port.connection.name))
					}*/
					this.out_port_list.add(new CdfdPort(false, variable_list, this.out_port_list.length, this))
				]
			}
		}
		if (this.in_port_list.length == 0) {
			this.in_port_list.add(new CdfdPort(true, new ArrayList<VariableName>, this.out_port_list.length, this))
		}
		if (this.out_port_list.length == 0) {
			this.out_port_list.add(new CdfdPort(true, new ArrayList<VariableName>, this.out_port_list.length, this))
		}
		this.max_port_count = Math.max(this.max_port_count, this.in_port_list.length)
		this.max_port_count = Math.max(this.max_port_count, this.out_port_list.length)

		//this.node_list.put(this.get_node_name, this)
		// add data stores
		if (module.var_decl !== null) {
			if (module.var_decl.vars !== null) {
				for (store : module.var_decl.vars) {
					this.store_list.add(store.name.name)
				}
			}
			if (module.var_decl.extVars !== null) {
				for (store : module.var_decl.extVars) {
					if (store?.variable_name?.name !== null)
						this.store_list.add(store.variable_name.name)
					else if (store?.ext_variable_name?.name !== null)
						this.store_list.add(store.ext_variable_name.name)
				}
			}
		}

		// add all data FlowIds
		if (module.cdfd !== null) {
			val cdsa_name = module.cdfd.name
			// for all flows
			for (flow : module.cdfd.flow) {
				val current_flow = new Flow(
					getNodeName(flow.source.node, module.cdfd),
					new VariableName(flow.source.variable_name),
					new VariableName(flow.destination.variable_name),
					getNodeName(flow.destination.node, module.cdfd),
					false
				)
				this.flow_list.add(current_flow)
			}
			for (flow : module.cdfd.shadow_flow) {
				val current_flow = new Flow(
					getNodeName(flow.source.node, module.cdfd),
					new VariableName(flow.source.connected_name),
					new VariableName(flow.destination.connected_name),
					getNodeName(flow.destination.node, module.cdfd),
					true
				)
				this.flow_list.add(current_flow)
			}

			// connect the flows
			if (this.flow_list !== null) {
				this.flow_list.forEach [ flow, index |
					if (cdsa_name == flow.src_name) {
						if(cdsa_name == flow.dest_name){
							this.add_node_as_destination(flow.src_var, this)
						}else{
							var dest_node = this.node_list.get(flow.dest_name)
							if (dest_node === null) {
								var str = '''Found node is null node:«flow.dest_name» var:«flow.dest_var»'''
								throw new RuntimeException(str)
							}
							this.add_node_as_destination(flow.src_var, dest_node)						
						}
					} else if (cdsa_name == flow.dest_name) {
						var src_node = this.node_list.get(flow.src_name)
						src_node.add_node_as_destination(flow.dest_var, this)
					} else {
						var src_node = this.node_list.get(flow.src_name)
						var dest_node = this.node_list.get(flow.dest_name)
						src_node.add_node_as_destination(flow.src_var, dest_node)
					}
				]
			}
			var ss = this.flow_list.toSet
			if(ss.length != this.flow_list.length){
				throw new RuntimeException('''duplciate flows exit «this.flow_list»''')
			}
			
		}
		
	}

	override add_node_as_destination(VariableName variable_name, NodeInterface node) {
		System.out.println("Module " + node.get_node_name)
		var out_port = this.get_containing_ports(variable_name, true)
		var in_port = node.get_containing_ports(variable_name, true)
		for(oport:out_port){
			for(iport:in_port){
				oport.add_associated_port(variable_name, iport)
				iport.add_associated_port(variable_name, oport)
			}			
		}
		
	}

	def get_nodes() {
		return node_list
	}

	def get_FlowIds() {
		return flow_list
	}

	override Iterable<Port>  get_containing_ports(VariableName variable_name, Boolean input) {
		if (input) { 
			var currrent_port_itr = this.in_port_list.filter([Port p|p.contain_variable(variable_name)])
			if (currrent_port_itr.length == 0) {
				System.out.println("Module name: " + this.module.name + ' look at input: ' + input)
				System.out.println(this.in_port_list.map[Port p|p.variables.map[v|v.variable_name]])
				System.out.println(
					"variables " + currrent_port_itr + " of length " + currrent_port_itr.length + " looking for " +
						variable_name + ' in module ' + this.toString)
				throw new RuntimeException("variables " + currrent_port_itr.length + " looking for " + variable_name)
			}
			return currrent_port_itr.map[it as Port]
		} else {
			var currrent_port_itr = this.out_port_list.filter([Port p|p.contain_variable(variable_name)])
			if (currrent_port_itr.length == 0) {
				System.out.println("Module name: " + this.module.name + ' look at input: ' + input)
				System.out.println(this.out_port_list.map[Port p|p.variables.map[v|v.variable_name]])
				System.out.println(
					"variables " + currrent_port_itr + " of length " + currrent_port_itr.length + " looking for " +
						variable_name)
				throw new RuntimeException("variables " + currrent_port_itr.length + " looking for " + variable_name)
			}
			return currrent_port_itr.map[it as Port]
		}
	}

	override get_node_name() {
		return this.cdsa_name
	}
	
	def getNodeName(NodeName node, SoflBehaviour cdfd){
		if(node.is_behavior){
			cdfd.name
		}else if(node.cond !== null){
			node.cond.name
		}else if(node.proc !== null){
			node.proc.name
		}else if(node.broadcast !== null){
			node.broadcast.name
		}else if(node.merge !== null){
			node.merge.name
		}else if(node.unmerge !== null){
			node.unmerge.name
		}else{
			"undefined"
		}
	}
	
	override toString() {
		var ret = this.name + '['
		for(p : this.in_port_list){
			ret = ret + p.toString + ','
		}
		ret = ret + ']['
		for(p : this.out_port_list){
			ret = ret + p.toString + ','
		}
		ret +']'
	}
	
	/*def static String getName(ConnectionName connect){
		'''connect «connect.name»'''
	}*/

}
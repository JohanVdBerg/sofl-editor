package com.gitlab.johanvdberg.sofl_editor.semantics

import java.util.List
import java.util.ArrayList

class PortVariable {
	
	public VariableName variable_name
	public List<Port> associated_ports
	
	
	new(VariableName var_name){
		this.associated_ports = new ArrayList<Port>
		this.variable_name = var_name
	}
	
	def void set_port(Port prt){
		if(prt === null){
			throw new RuntimeException('Error')
		}
		this.associated_ports.add(prt)
	}
	
	override toString() {
		this.variable_name.name
	}
	
} 

 
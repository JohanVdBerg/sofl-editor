package com.gitlab.johanvdberg.sofl_editor.semantics

import com.gitlab.johanvdberg.sofl_editor.generator.ProcAlgGenerate
import java.util.ArrayList
import java.util.List
import java.util.TreeSet

class Port {

	protected List<PortVariable> variables
	protected int index_of_port
	protected boolean inport;
	public boolean is_connected
	//static protected String empty_variable_name_template = 'connected'
	public NodeInterface parent

	new(boolean inport, List<VariableName> variables, int port_index, NodeInterface parent) {
		this.variables = new ArrayList<PortVariable>
		this.index_of_port = port_index
		this.inport = inport
		this.parent = parent
		if (this.parent === null) {
			throw new Exception
		}
		variables.forEach[VariableName variable_name|this.variables.add(new PortVariable(variable_name))]
		this.is_connected = false
	}

	//create an empty connected port
	new(boolean inport, VariableName connected_name, NodeInterface parent) {
		this.variables = new ArrayList<PortVariable>
		this.index_of_port = port_index
		this.inport = inport
		this.parent = parent
		if (this.parent === null) {
			throw new Exception
		}

		//this.variables.add(new PortVariable(Port.empty_variable_name(connected_name)))
		this.variables.add(new PortVariable(connected_name))
		this.is_connected = true
	}

	/*static def empty_variable_name(VariableName index){
		return Port.empty_variable_name_template + index
	}*/
	def boolean contain_variable(VariableName to_find) {
		//System.out.println('''look for «to_find» in port with «this»''')
		var ret = this.variables.exists[PortVariable port|port.variable_name == to_find]
		//System.out.println('''results: «ret»''')
		return ret
	}

	def add_associated_port(VariableName variable_name, Port port) {
		var prt = this.variables.findFirst[PortVariable c_prt|c_prt.variable_name == variable_name]
		prt.set_port(port)
	}

	def List<Flow> get_flow_list() {
		val ret = if (this.inport) {
			this.variables.filter[itr|itr.associated_ports.length > 0].toList.map[it| 
				it.associated_ports.map[
					port|new Flow(port.parent_name, it.variable_name, it.variable_name, this.parent_name)
			]].flatten
		} else {
			this.variables.filter[itr|itr.associated_ports.length > 0].toList.map [it|
				it.associated_ports.map[port|
					new Flow(this.parent_name, it.variable_name, it.variable_name, port.parent_name)
			]].flatten
		}
		
		var s = new TreeSet<Flow>
		s.addAll(ret) 
		val ret_values = new ArrayList<Flow>
		s.forEach[it| ret_values.add(it)]
		return ret_values
	}

	def port_index() {
		return index_of_port
	}

	def name() {
		return ProcAlgGenerate.port_name(index_of_port)
	}

	def variable_list() {
		return variables
	}

	def parent_name() {
		return this.parent.get_node_name
	}
	
	override toString() {
		var ret = '{'
		for(p:this.variables){
			ret = ret + p.toString + ','
		}
		return ret + '}'
	}
	
}
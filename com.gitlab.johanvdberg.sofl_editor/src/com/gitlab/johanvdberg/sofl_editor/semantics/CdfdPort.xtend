package com.gitlab.johanvdberg.sofl_editor.semantics

import java.util.List
import java.util.TreeSet
import java.util.ArrayList

class CdfdPort extends Port {
	
	new(boolean inport, List<VariableName> variables, int port_index, NodeInterface parent) {
		super(inport, variables, port_index, parent)
	}
	
	override List<Flow> get_flow_list() {
		val ret = if (!this.inport) {
			this.variables.filter[itr|itr.associated_ports.length > 0].toList.map[it| 
				it.associated_ports.map[
					port|new Flow(port.parent_name, it.variable_name, it.variable_name, this.parent_name)
			]].flatten
		} else {
			this.variables.filter[itr|itr.associated_ports.length > 0].toList.map [it|
				it.associated_ports.map[port|
					new Flow(this.parent_name, it.variable_name, it.variable_name, port.parent_name)
			]].flatten
		}
		
		var s = new TreeSet<Flow>
		s.addAll(ret) 
		val ret_values = new ArrayList<Flow>
		s.forEach[it| ret_values.add(it)]
		return ret_values
	}
}
package com.gitlab.johanvdberg.sofl_editor.semantics

import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConnectionName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflVariableName

class VariableName {
	public String name
	public boolean shadow
	/*new(String name, boolean shadow){
		this.name = name
		this.shadow = shadow
		if(shadow){
			this.name = 'shadow' + this.name
		}
	}*/
	
	new(ConnectionName name) {
		this.name = name.name
		this.shadow = true
	}
	
	new (SoflVariableName name){
		this.name = name.name
		this.shadow = false
	}
	
	override equals(Object obj) {
		if(obj instanceof VariableName){
			return this.name == obj.name
		}
		super.equals(obj)
	}
	
	override toString() {
		this.name
	}
	
}
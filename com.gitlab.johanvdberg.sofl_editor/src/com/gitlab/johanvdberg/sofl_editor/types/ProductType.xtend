package com.gitlab.johanvdberg.sofl_editor.types

import java.util.ArrayList
import java.util.List

class ProductType extends TypeBase{
	ArrayList<TypeBase> type_list
	
	new(List<TypeBase>in_list){
		super(TypeBase::PROCESS_TYPE)
		this.type_list = new ArrayList<TypeBase>	
		this.type_list.addAll(in_list)	
	}
	
	
	def getTypeSet(){
		new PossibleSetOfTypes(this.type_list.toSet)
	}
	
	def getTypeCount(){
		return this.type_list.length
	}
	
	def getTypeAt(int index){
		return this.type_list.get(index)
	}	
	
	override equals(Object o){
		if(o instanceof ProductType){
			return (this.type_list == o.type_list)
		}
		return super.equals(o)
	}
	
	override toString(){
		'''prod(«FOR port:this.type_list SEPARATOR ','»«port»«ENDFOR»)'''
	}
	
}

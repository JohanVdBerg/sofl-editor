package com.gitlab.johanvdberg.sofl_editor.types

import java.util.List
import java.util.ArrayList

class EnumerateType extends TypeBase{
	
	List<String> elements
	
	new(List<String> elements){
		super(TypeBase::ENUM_TYPE)
		this.elements = new ArrayList<String>
		this.elements.addAll(elements)		
	}
	
	
	override toString(){
		'''enumerate «FOR el : this.elements SEPARATOR ','»«el»«ENDFOR»'''
	}
	
	override equals(Object o){
		if(o instanceof EnumerateType){
			return this.elements == o.elements
		}else{
			return false		
		}
	}
	
}
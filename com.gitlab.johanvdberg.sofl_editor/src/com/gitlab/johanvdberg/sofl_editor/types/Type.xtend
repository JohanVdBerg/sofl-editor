package com.gitlab.johanvdberg.sofl_editor.types

import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflType
import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.soflDsl.TypeLower
import org.eclipse.xtext.EcoreUtil2
import com.gitlab.johanvdberg.sofl_editor.soflDsl.NewType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflFunction
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConnectionName
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ParamDecl
import com.gitlab.johanvdberg.sofl_editor.soflDsl.DataFlows

class Type {
	def static TypeBase getType(SoflType type) {
		if (type === null) {
			throw new RuntimeException()
		}
		var tmp = getType(type.left)
		if (tmp.is_error) {
			return tmp
		} else if (type.right_product.length != 0) {
			var lst = type.right_product.map[getType(it)]
			var ll = new ArrayList<TypeBase>()
			ll.add(tmp)
			ll.addAll(lst)
			return new ProductType(ll)
		} else {
			return tmp
		}
	}

	def static TypeBase getType(TypeLower type) {
		if (type?.basic !== null) {
			return switch type.basic {
				case type.basic.is_bool:
					new BooleanType()
				case type.basic.is_char:
					new CharacterType()
				case type.basic.is_int:
					new IntegerType()
				case type.basic.is_nat:
					new NaturalType()
				case type.basic.is_nat0:
					new Natural0Type()
				case type.basic.is_real:
					new RealType()
				/*case type.basic.is_string:
				 new SeqType(new CharacterType())*/
				case type.basic.enumerate !== null:
					new EnumerateType(type.basic.enumerate.enum_values.map[a|'''<«a.name»>'''])
				case type.basic.set !== null:
					new SetType(getType(type.basic.set.type))
				case type.basic.composed !== null: {
					var tmp = if (type.basic.composed.parent !== null) {
							//var parent = getType(EcoreUtil2.getContainerOfType(type.basic.composed.parent, SoflType))
							var tmp_parent_declare = EcoreUtil2.getContainerOfType(type.basic.composed.parent, NewType)
							var parent = getType(tmp_parent_declare.type)
							if (parent.is_error) {
								return parent
							}
							new ComposedType(parent as ComposedType)
						} else {
							new ComposedType()
						}

					if (!tmp.is_error) {
						for (el : type.basic.composed.element_list) {
							tmp.add(el.name, getType(el.type))
						}
					}
					tmp
				}
				case type.basic.map !== null:
					new MapType(getType(type.basic.map.domain_type), getType(type.basic.map.range_type))
				case type.basic.sequence !== null:
					new SeqType(getType(type.basic.sequence.type))
				default:
					new ErrorType("Not defined TypeFactory::getType")
			}
		} else if (type?.type_id !== null) {
			getType((type.type_id.eContainer as NewType).type)
		} else {
			new ErrorType("Not defined TypeFactory::getType")
		}
	}

	def static TypeBase getType(SoflFunction function) {
		new FunctionType(getTypeList(function.in_ports), getType(function.ret_type))
	}

	def static TypeBase getType(SoflProcess process) {
		new ProcessType(getTypeList(process.in_ports), getTypeList(process.out_ports))
	}

	def static getType(SoflBehaviour t) {
		new BehaviorType(getTypeList(t.in_ports), getTypeList(t.out_ports))
	}

	def static getType(ConnectionName connect) {
		return new ConnectionType()
	}

	def static getTypeList(ParamDecl parameters) {
		var ret = new ArrayList<TypeBase>
		for (el : parameters.list) {
			var t_type = getType(el.type)
			for (var i = 0; i < el.list.length; i = i + 1) {
				ret.add(t_type)
			}
		}
		return ret
	}

	def static getTypeList(DataFlows parameters) {
		var ret = new ArrayList<ArrayList<TypeBase>>
		for (port : parameters.ports) {
			val port_elems = new ArrayList<TypeBase>
			port.connections.forEach[port_elems.add(new ConnectionType)]
			/*if(port.connection !== null){
			 * 	port_elems.add(getType(port.connection))
			 }else */
			if (port.declare.length > 0) {
				for (el : port.declare) {
					var type = getType(el.type)
					for (var i = 0; i < el.identifier.length; i = i + 1) {
						port_elems.add(type)
					}
				}
			}
			ret.add(port_elems)
		}
		return ret
	}
}

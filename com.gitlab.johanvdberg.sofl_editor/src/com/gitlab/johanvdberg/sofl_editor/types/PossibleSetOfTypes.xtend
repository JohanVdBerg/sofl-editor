package com.gitlab.johanvdberg.sofl_editor.types

import java.util.Set
import java.util.HashSet

class PossibleSetOfTypes extends TypeBase {
	
	Set<TypeBase> possibleTypes
	new(Set<TypeBase> type_options) {
		super(TypeBase::SET_OF_POSIIBLE_TYPE)
		this.possibleTypes = new HashSet<TypeBase>
		this.possibleTypes.addAll(type_options)
	}
	
	override equals(Object o){
		if(o instanceof PossibleSetOfTypes){
			//return true
			var intersec = this.possibleTypes.filter[o.possibleTypes.contains(it)]
			return intersec.length > 0
		}else{
			return this.possibleTypes.exists[it.equals(o)]
		}
	}
	
}
package com.gitlab.johanvdberg.sofl_editor.types

import com.gitlab.johanvdberg.sofl_editor.soflDsl.BroadcastStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ConditionStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.Expression
import com.gitlab.johanvdberg.sofl_editor.soflDsl.FlowDestination
import com.gitlab.johanvdberg.sofl_editor.soflDsl.FlowSource
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflFunction
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflVariableName
import java.util.ArrayList
import java.util.HashSet
import java.util.List
import org.eclipse.xtext.EcoreUtil2
import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure

class NodeBaseType extends TypeBase {
	public List<HashSet<VariableType>> in_types
	public List<HashSet<VariableType>> out_types

	def static create(BroadcastStructure node) { new BroadcastType(node) }

	def static create(ConditionStructure node) { new ConditionType(node) }

	protected new(String type) {
		super(type)
		this.in_types = new ArrayList<HashSet<VariableType>>
		this.out_types = new ArrayList<HashSet<VariableType>>
	}

	static def TypeBase getType(FlowSource flow) {
		if (flow.node.is_behavior) {
			getType(flow.variable_name)
		} else if (flow.node.broadcast !== null) {
			getType(flow.node.broadcast, false, flow.variable_name)
		} else if (flow.node.cond !== null) {
			getType(flow.node.cond, false, flow.variable_name)
		} else if (flow.node.proc !== null) {
			ExpressionType::getType(flow.variable_name)
		} else if (flow.node.merge !== null) {
			getType(flow.node.merge, false, flow.variable_name)
		} else if (flow.node.unmerge !== null) {
			getType(flow.node.unmerge, false, flow.variable_name)
		} else {
			throw new RuntimeException("error")
		}
	}

	static def TypeBase getType(FlowDestination flow) {
		if (flow.node.is_behavior) {
			getType(flow.variable_name)
		} else if (flow.node.broadcast !== null) {
			getType(flow.node.broadcast, true, flow.variable_name)
		} else if (flow.node.cond !== null) {
			getType(flow.node.cond, true, flow.variable_name)
		} else if (flow.node.proc !== null) {
			ExpressionType::getType(flow.variable_name)
		} else if (flow.node.merge !== null) {
			getType(flow.node.merge, true, flow.variable_name)
		} else if (flow.node.unmerge !== null) {
			getType(flow.node.unmerge, true, flow.variable_name)
		} else {
			throw new RuntimeException("error")
		}
	}

	static def TypeBase getType(MergeStructure node, boolean name_destination, SoflVariableName name) {
		var behaviour = EcoreUtil2.getContainerOfType(node.eContainer, SoflBehaviour)
		var canidates = behaviour.flow.filter[it.destination.node.merge !== null]
		var canidates_source = canidates.filter[it.destination.node.broadcast.name == node.name].map[it.source]
		if (canidates_source.length != 1) {
			throw new RuntimeException('''Error «canidates.length»''')
		}
		var current = canidates_source.head
		getType(current)
	}

	static def TypeBase getType(UnMergeStructure node, boolean name_destination, SoflVariableName name) {
		var behaviour = EcoreUtil2.getContainerOfType(node.eContainer, SoflBehaviour)
		var canidates = behaviour.flow.filter[it.destination.node.unmerge !== null].filter [
			it.destination.node.broadcast.name == node.name
		].map[it.source]
		if (canidates.length != 1) {
			throw new RuntimeException('''Error «canidates.length»''')
		}
		var current = canidates.head
		getType(current)
	}

	static def TypeBase getType(BroadcastStructure node, boolean name_destination, SoflVariableName name) {
		var behaviour = EcoreUtil2.getContainerOfType(node.eContainer, SoflBehaviour)
		var canidates = behaviour.flow.filter[it.destination.node.broadcast !== null].filter [
			it.destination.node.broadcast.name == node.name
		].map[it.source]
		if (canidates.length != 1) {
			new ErrorType('''Error «canidates.length»''')
		} else {
			var current = canidates.head
			getType(current)
		}
	}

	static def TypeBase getType(ConditionStructure node, boolean name_destination, SoflVariableName name) {
		var behaviour = EcoreUtil2.getContainerOfType(node.eContainer, SoflBehaviour)
		var canidates_structure = behaviour.flow.filter[it.destination.node.cond !== null].toList
		var canidates = canidates_structure.filter[it.destination.node.cond.name == node.name].map[it.source]
		if (canidates.length != 1) {
			var str = '''Error «canidates.length»'''
			//throw new RuntimeException(str)
			new ErrorType(str)
		}
		var current = canidates.head
		getType(current)
	}

	def static getType(Expression t) { ExpressionType::getType(t) }

	def static getType(SoflVariableName t) { ExpressionType::getType(t) }

	def static getType(SoflType t) { ExpressionType::getType(t) }

	def static getType(SoflProcess t) { Type::getType(t) }

	def static getType(SoflFunction t) { Type::getType(t) }

	def static getType(SoflBehaviour t) { Type::getType(t) }
}

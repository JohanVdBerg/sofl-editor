package com.gitlab.johanvdberg.sofl_editor.types

import java.util.List
import java.util.ArrayList

class FunctionType extends TypeBase {
	List<TypeBase> parameters
	TypeBase return_type
	new(List<TypeBase> parameters, TypeBase return_element){
		super(TypeBase::FUNCTION_TYPE)
		this.parameters = new ArrayList<TypeBase>
		this.parameters.addAll(parameters)
		this.return_type = return_element
	}
	
	def get_parameters(){
		return parameters
	}
	
	def get_return_type(){
		return return_type
	}
	
}

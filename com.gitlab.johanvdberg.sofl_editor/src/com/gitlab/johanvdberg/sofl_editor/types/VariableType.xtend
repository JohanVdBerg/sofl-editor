package com.gitlab.johanvdberg.sofl_editor.types

import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflType
import com.gitlab.johanvdberg.sofl_editor.soflDsl.PortDeclarations
import java.util.ArrayList
import com.gitlab.johanvdberg.sofl_editor.soflDsl.DataFlows

class VariableType implements Comparable<VariableType>{
		public TypeBase type
	public String varaible
	
	
	new(String varaible, TypeBase type){
		this.type = type
		this.varaible = varaible
	}
	
	new(String varaible){
		this.type = null
		this.varaible = varaible
	}

	def static getType(SoflType type){
		ExpressionType.getType(type)
	}
	
	
	override compareTo(VariableType o) {
		if(this.varaible < o.varaible){
			-1
		}else if(this.varaible > o.varaible){
			1
		}else{
			if(this.type !== null && o.type !== null){
				this.type.compareTo(o.type)
			}else{
				0
			}
		}
	}
	
	def static create(PortDeclarations port){
		var v = (
				port.connections.map[new VariableType(it.name, new ConnectionType)] + 
				port.declare.map[it.identifier.map[v |new VariableType(v.name, getType(it.type))]].flatten)
		new ArrayList(v.toList)
	}
	
	def static create(DataFlows ports){
		var lst = ports.ports.map[it|
			create(it)
		]
		lst
	}
	
	override toString(){
		'''(«this.varaible» «this.type»)'''
	}
}
	

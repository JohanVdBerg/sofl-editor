package com.gitlab.johanvdberg.sofl_editor.types

class NumericType extends PrimitiveType {

	new(String type) {
		super(type)
	}

	override equals(Object o) {
		if (o instanceof NumericType) {
			// return true
			return this.typeClass == o.typeClass
		}
		return super.equals(o)
	}

	def get_larger_of_the_two(NumericType other) {
		if (this == other) {
			this
		} else if (this instanceof Natural0Type) {
			other
		} else if (other instanceof Natural0Type) {
			this
		} else if (other instanceof NaturalType) {
			this
		} else if (this instanceof NaturalType) {
			other
		} else if (this instanceof IntegerType) {
		 	other
		 } else if (other instanceof IntegerType) {
		 	this
		} else {
			 this
		}
	}
}

package com.gitlab.johanvdberg.sofl_editor.types

import com.gitlab.johanvdberg.sofl_editor.soflDsl.MergeStructure
import java.util.HashSet


class MergeType extends NodeBaseType {
	protected new(MergeStructure node) {
		super(TypeBase::MERGE_TYPE)
		val v_type = node.invar.map[new VariableType(it.name, getType(node, true, it))]
		this.in_types.addAll(#[new HashSet(v_type)])
		this.out_types.add(newHashSet(#[new VariableType(node.out.name, new ProductType(v_type.map[it.type]))]))
	}
}

package com.gitlab.johanvdberg.sofl_editor.types

import com.gitlab.johanvdberg.sofl_editor.soflDsl.UnMergeStructure
import java.util.HashSet

class UnMergeType extends NodeBaseType {
	protected new(UnMergeStructure node) {
		super(TypeBase::MERGE_TYPE)
		val v_type = new VariableType(node.invar.name, getType(node, true, node.invar))
		this.in_types.addAll(#[new HashSet(#[v_type])])
		var v_out = new HashSet<VariableType>
		var type = v_type.type as ProductType
		for(index:0..<type.getTypeCount()){
			v_out.add(
				new VariableType(
					node.out_list.get(index).name, 
					type.getTypeAt(index))
				)			
		}
		this.out_types.add(v_out)
	}
}

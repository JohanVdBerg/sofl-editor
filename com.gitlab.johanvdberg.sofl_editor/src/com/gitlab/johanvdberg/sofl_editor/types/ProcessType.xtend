package com.gitlab.johanvdberg.sofl_editor.types

import java.util.List
import java.util.ArrayList

class ProcessType extends TypeBase{
	public ArrayList<PortType> in_list
	public ArrayList<PortType> out_list
	
	new(List<ArrayList<TypeBase>>in_list, List<ArrayList<TypeBase>> out_list){
		super(TypeBase::PROCESS_TYPE)
		this.in_list = new ArrayList<PortType>
		this.out_list = new ArrayList<PortType>
		
		for(out:out_list){
			var pt = new PortType
			pt.addAll(out)
			this.out_list.add(pt)
		}
		for(inl:in_list){
			var pt = new PortType
			pt.addAll(inl)
			this.out_list.add(pt)
		}
	
	}	

	
	override equals(Object o){
		if(o instanceof ProcessType){
			return (this.in_list == o.in_list) && (this.out_list == o.out_list)
		}else if(o instanceof BehaviorType){
			return (this.in_list == o.in_list) && (this.out_list == o.out_list)
		}
		return false
	}
	
	override toString(){
		'''proc([«FOR port:this.in_list SEPARATOR ','»[«port»]«ENDFOR»],[«FOR port:this.out_list SEPARATOR ','»[«port»]«ENDFOR»])'''
	}
}

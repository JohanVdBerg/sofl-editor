package com.gitlab.johanvdberg.sofl_editor.types

class Natural0Type extends NumericType{
	
	new(){
		super(TypeBase::NAT0_TYPE)
	}
	
}

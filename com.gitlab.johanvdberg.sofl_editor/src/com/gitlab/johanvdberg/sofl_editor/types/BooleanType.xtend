package com.gitlab.johanvdberg.sofl_editor.types

class BooleanType extends PrimitiveType{
	
	new(){
		super(TypeBase::BOOL_TYPE)
	}
	
}
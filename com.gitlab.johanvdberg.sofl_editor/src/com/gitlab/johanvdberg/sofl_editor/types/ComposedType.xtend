package com.gitlab.johanvdberg.sofl_editor.types

import java.util.HashMap
import java.util.List

class ComposedType extends TypeBase{
	
	HashMap<String, TypeBase> contains
	
	new(){
		super(TypeBase::COMP_TYPE)
		this.contains = new HashMap<String, TypeBase>()
	}
	
	new(ComposedType t){
		super(TypeBase::COMP_TYPE)
		this.contains = new HashMap<String, TypeBase>()
		for(key:t.contains.keySet()){
			this.contains.put(key, t.contains.get(key))
		}
	}
	def add(String name, TypeBase type){
		if(this.contains.containsKey(name)){
			throw new RuntimeException("duplicate name in composed type")
		}
		this.contains.put(name, type)
	}
	
	def get_type_of(String name){
		if(this.contains.containsKey(name)){
			return this.contains.get(name)
		}else{
			return new ErrorType('''«this» does not contain member «name»''')
		}
	}

	def can_create_from(List<TypeBase> parameters){
		true
	}
}
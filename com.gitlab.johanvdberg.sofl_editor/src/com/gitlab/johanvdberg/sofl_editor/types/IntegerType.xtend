package com.gitlab.johanvdberg.sofl_editor.types

class IntegerType extends NumericType{
	
	new(){
		super(TypeBase::INT_TYPE)
	}
	
}

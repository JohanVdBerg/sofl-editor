package com.gitlab.johanvdberg.sofl_editor.types


class MapType extends TypeBase {
	
	TypeBase domain
	TypeBase range
	new(TypeBase domain, TypeBase range){
		super(TypeBase::MAP_TYPE)
		this.domain = domain
		this.range = range
		
	}
	
	override equals(Object o){
		if(o instanceof MapType){
			return (this.domain == o.domain) && (this.range == o.range)
		}else{
			return false		
		}
	}	
	
	def get_domain_type(){
		return domain
	}
	
	def get_range_type(){
		return range
	}
	
}

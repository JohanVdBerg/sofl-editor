/*
 * generated by Xtext 2.14.0
 */
package com.gitlab.johanvdberg.sofl_editor.ui.outline

import org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflProcess
import org.eclipse.xtext.ui.label.StylerFactory
import org.eclipse.xtext.ui.PluginImageHelper
import org.eclipse.xtext.ui.editor.outline.impl.DocumentRootNode
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModelList
import com.google.inject.Inject
import org.eclipse.xtext.ui.editor.outline.IOutlineNode
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflModule
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflDslPackage
import com.gitlab.johanvdberg.sofl_editor.soflDsl.SoflBehaviour
import com.gitlab.johanvdberg.sofl_editor.soflDsl.ProcessVariable
import com.gitlab.johanvdberg.sofl_editor.soflDsl.PortDeclarations

/**
 * Customization of the default outline structure.
 *
 * See https://www.eclipse.org/Xtext/documentation/310_eclipse_support.html#outline
 */
class SoflDslOutlineTreeProvider extends DefaultOutlineTreeProvider {
	@Inject StylerFactory stylerFactory;
	@Inject PluginImageHelper images_helper;

	def void _createChildren(DocumentRootNode outlineNode, SoflModelList model) {
		model.modules.forEach [ element |
			createNode(outlineNode, element)
		]
	}

	def void _createChildren(IOutlineNode outlineNode, SoflModule model) {
		if (model.proc_spec !== null) {
			model.proc_spec.forEach [ element |
				createNode(outlineNode, element)
			]
		}
		if (model.cdfd !== null) {
			createNode(outlineNode, model.cdfd)
		}
	}

	def void _createChildren(IOutlineNode outlineNode, SoflProcess model) {
		val in_node = createEStructuralFeatureNode(outlineNode, model, SoflDslPackage.Literals.SOFL_PROCESS__IN_PORTS,
			images_helper.getImageDescriptor(images_helper.defaultImage), "Input", false)
		model.in_ports.ports.forEach [ element |
			createNode(in_node, element)
		]
		val out_node = createEStructuralFeatureNode(outlineNode, model, SoflDslPackage.Literals.SOFL_PROCESS__OUT_PORTS,
			images_helper.getImageDescriptor(images_helper.defaultImage), "Output", false)
		if (model?.out_ports?.ports !== null) {
			model.out_ports.ports.forEach [ element |
				createNode(out_node, element)
			]
		}
		if (model.ext !== null) {
			val ext_node = createEStructuralFeatureNode(outlineNode, model, SoflDslPackage.Literals.SOFL_PROCESS__EXT,
				images_helper.getImageDescriptor(images_helper.defaultImage), "external stores", false)
			model.ext.variables.forEach [ element |
				createNode(ext_node, element)
			]
		}
	}

	def void _createChildren(IOutlineNode outlineNode, SoflBehaviour model) {
		val in_node = createEStructuralFeatureNode(outlineNode, model, SoflDslPackage.Literals.SOFL_PROCESS__IN_PORTS,
			images_helper.getImageDescriptor(images_helper.defaultImage), "Input", false)
		model.in_ports.ports.forEach [ element |
			createNode(in_node, element)
		]
		val out_node = createEStructuralFeatureNode(outlineNode, model, SoflDslPackage.Literals.SOFL_PROCESS__OUT_PORTS,
			images_helper.getImageDescriptor(images_helper.defaultImage), "Output", false)
		model.out_ports.ports.forEach [ element |
			createNode(out_node, element)
		]
	}

	def void _createChildren(IOutlineNode outlineNode, PortDeclarations parameter) {

		/*if (parameter.not_connected) {
		 * 	createEStructuralFeatureNode(outlineNode, parameter,
		 * 		DslPackage.Literals.DATA_FLOW_PORT_DECLARATIONS__NOT_CONNECTED,
		 * 		images_helper.getImageDescriptor(images_helper.defaultImage), "empty", true)
		 * } else 
		 * 
		 * if (parameter.connection !== null) {
		 * 	createEStructuralFeatureNode(outlineNode, parameter,
		 * 		DslPackage.Literals.DATA_FLOW_PORT_DECLARATIONS__CONNECTION,
		 * 		images_helper.getImageDescriptor(images_helper.defaultImage), "shadow: " + parameter.connection.name,
		 true)*/
		parameter.connections.forEach [
			createEStructuralFeatureNode(outlineNode, parameter,
				SoflDslPackage.Literals.PORT_DECLARATIONS__CONNECTIONS,
				images_helper.getImageDescriptor(images_helper.defaultImage), "shadow: " + it.name, true)
		]
		// } else {
		parameter.declare.forEach [ element |
			element.identifier.forEach [ variable |
				createEStructuralFeatureNode(outlineNode, parameter, SoflDslPackage.Literals.SOFL_VARIABLE_NAME__NAME,
					images_helper.getImageDescriptor(images_helper.defaultImage), variable.name, true)
			]
		]
	// }
	}

	def _isLeave(SoflProcess proc) { return false }

	def Object _text(SoflProcess entity) {
		return "process: " + entity.name
	}

	def Object _text(SoflBehaviour entity) {
		return "behaviour: " + entity.name
	}

	def Object _text(PortDeclarations entity) {
		return "port"
	}

	def Object _text(ProcessVariable entity) {
		return if (entity.rd) {
			"read: "
		} else
			{
				"write: "
			} + entity.varariable_name.name
	}

}
